name := "event-nugget"

version := "1.1"

scalaVersion := "2.11.7"

scalacOptions ++= Seq(
    "-deprecation",
    "-feature",
    "-unchecked"
)

fork := true

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
resolvers += Resolver.sonatypeRepo("public")

libraryDependencies ++= Seq(
    "org.scala-lang.modules" %% "scala-xml" % "1.0.5" withSources() withJavadoc(),
    "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.3" withSources() withJavadoc(),
    "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test" withSources() withJavadoc(),
    "com.typesafe" % "config" % "1.3.0" withSources() withJavadoc(),
    "net.ceedubs" %% "ficus" % "1.1.2" withSources() withJavadoc(),
    "commons-cli" % "commons-cli" % "1.3.1" withSources() withJavadoc(),
    "commons-io" % "commons-io" % "2.4" withSources() withJavadoc(),
    "com.github.scopt" %% "scopt" % "3.3.0" withSources() withJavadoc(),
    "de.bwaldvogel" % "liblinear" % "1.95" withSources() withJavadoc(),
    "edu.stanford.nlp" % "stanford-corenlp" % "3.5.2" withSources() withJavadoc() artifacts(Artifact("stanford-corenlp", "models"), Artifact("stanford-corenlp"))
)
