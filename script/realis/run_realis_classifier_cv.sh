#!/usr/bin/env bash
set -e

working_dir="$(pwd)"
cd "$(dirname ${0})/../.."

config="script/realis/realis.conf"
output="result.$(date +'%Y-%m-%d-%H%M').cv"

sbt -mem 20480 "run-main realis.svm.RealisClassifierCrossValidator -c \"${config}\" -o \"${output}\""