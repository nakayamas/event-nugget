#!/usr/bin/env bash
set -e

working_dir="$(pwd)"
cd "$(dirname ${0})/../.."

sbt -mem 10240 "run-main realis.statistics.RealisStatistics"
