#!/usr/bin/env bash
set -e

working_dir="$(pwd)"

cd "$(dirname ${0})/../.."

config="script/realis/realis.conf"
output="result.$(date +'%Y-%m-%d-%H%M').rule-based"

sbt -mem 10240 "run-main realis.rule.RuleBasedRealisClassifier -c \"${config}\" -t test -o \"${output}\""
