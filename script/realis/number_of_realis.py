#!/usr/bin/env python
# encoding: utf-8

import sys, os
from xml.etree.ElementTree import ElementTree
from collections import defaultdict

nugget_dir = sys.argv[1]

num_of_realis = defaultdict(lambda : defaultdict(int))
all_num_of_realis = defaultdict(int)
for f in os.listdir(nugget_dir):
    tree = ElementTree()
    tree.parse(nugget_dir + "/" + f)

    doc_id = tree.getroot().attrib["doc_id"]
    domain = tree.getroot().attrib["source_type"]
    for em in tree.findall("event_mention"):
        realis = em.attrib["realis"]
        num_of_realis[domain][realis] += 1
        all_num_of_realis[realis] += 1

for domain, realis in num_of_realis.items():
    print "[" + domain + "]"
    s = sum(realis.values())
    for r, n in realis.items():
        print str(r) + "\t" + str(n) + "\t" + str(round(float(n) / float(s) * 100, 2))
    print "sum" + "\t" + str(s)
    print

print "[all domain]"
all_sum = sum(all_num_of_realis.values())
for all_realis, all_num in all_num_of_realis.items():
    print str(all_realis) + "\t" + str(all_num) + "\t" + str(round(float(all_num) / float(all_sum) * 100, 2))
print "sum" + "\t" + str(all_sum)
