#!/usr/bin/env bash
set -e

working_dir="$(pwd)"
cd "$(dirname ${0})/../.."

config="script/coreference/coreference.conf"
output="result.$(date +'%Y-%m-%d-%H%M')"

sbt -mem 20480 "run-main coreference.CoreferenceResolver -c \"${config}\" --param-tune -o \"${output}\""

publish_dir="${HOME}/public_html/event_nugget"
[ -e "${publish_dir}" ] && rm -rf "${publish_dir}"
mkdir "${publish_dir}"
for d in $(find "${output}" -name visualization -type d); do
    rsync -a "${d}/" "${publish_dir}/$(echo "${d}" | sed -e "s/.*\(n[0-9]\).*/\1/")"
done
