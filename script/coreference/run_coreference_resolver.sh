#!/usr/bin/env bash
set -e

working_dir="$(pwd)"
cd "$(dirname ${0})/../.."

config="script/coreference/coreference.conf"
output="result.$(date +'%Y-%m-%d-%H%M')"

sbt -mem 20480 "run-main coreference.CoreferenceResolver -c \"${config}\" --params cost=0.01,bias=-1 -o \"${output}\""
