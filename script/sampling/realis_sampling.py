#!/usr/bin/env python
# encoding: utf-8

import sys, os, random, re
from xml.etree.ElementTree import ElementTree
from itertools import groupby
from collections import defaultdict

def grouping(l, key=None):
    return groupby(sorted(l, key=key), key=key)


# Read
data_dir = sys.argv[1] # LDC2015E73_TAC_KBP_2015_Event_Nugget_Training_Data_Annotation_V2/data/event_nugget
genre_file = sys.argv[2] # LDC2015E73_TAC_KBP_2015_Event_Nugget_Training_Data_Annotation_V2/docs/event_nugget_stats.tab

brat_url = ""
if len(sys.argv) == 4:
    brat_url = sys.argv[3] # http://example.com/index.xhtml#/

genres = {}
for line in open(genre_file, "r"):
    file_name, genre, _, _, _ = line.split("\t")
    genres[file_name] = genre

events = defaultdict(lambda : defaultdict(list))
for f in os.listdir(data_dir):
    tree = ElementTree()
    tree.parse(data_dir + "/" + f)

    doc_id = tree.getroot().attrib["doc_id"]
    genre = genres[doc_id]
    for em in tree.findall("event_mention"):
        event = [doc_id]
        trigger = em.find("trigger")
        event.append(trigger.attrib["offset"])
        event_id = em.attrib["id"]
        event.append(event_id)
        event.append(trigger.text)
        if brat_url:
            event.append(brat_url + doc_id + "?focus=E" + re.search(r"em-(\d+)", event_id).group(1))
        realis = em.attrib["realis"]
        events[genre][realis].append("\t".join(event))

# Random sampling
random.seed(1)
for genre, genre_events in sorted(events.items(), key=lambda (g, _): g):
    print "------------ " + genre + " ------------"
    for realis, realis_events in sorted(genre_events.items(), key=lambda (r, _): r):
        print "[" + realis + "]"
        for s in sorted(random.sample(realis_events, 10)):
            print s
        print
    print