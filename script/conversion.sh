#!/usr/bin/env zsh

project_dir="${"$(dirname "${0}")"%%/sh}"
working_dir="$(pwd)"

data_dir="${project_dir}/data/train_data"
ldc_text_dir="${data_dir}/source"
ldc_ann_dir="${data_dir}/event_hopper"
brat_output_dir="${data_dir}/ann"
token_table_dir="${data_dir}/tkn"
output_tbf_basename="${data_dir}/tbf/gold"

java -jar "${project_dir}/EvmEval/bin/converter-1.0.3-jar-with-dependencies.jar"\
    -t ldc_text_dir\
    -te txt\
    -a ldc_ann_dir\
    -ae event_hoppers.xml\
    -o brat_output_dir

java -jar "${project_dir}/EvmEval/bin/token-file-maker-1.0.4-jar-with-dependencies.jar"\
    -a brat_output_dir\
    -t brat_output_dir\
    -e txt\
    -o token_table_dir

python "${project_dir}/EvmEval/brat2tbf.py"\
    -t token_table_dir\
    -d brat_output_dir\
    -o output_tbf_basename\
    -w