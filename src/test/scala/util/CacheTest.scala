package util

import java.io.File

import config.EventNuggetConfig
import doc.{Doc, Tkn}
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}
import reader.TknFileReader

import scala.collection.immutable.IndexedSeq
import scala.io.Source

/**
  * Created by nakayama.
  */
class CacheTest extends FlatSpec with Matchers with PrivateMethodTester {
    val tknFile = new File("src/test/resources/event_nugget/news1.tab")

    val doc: Doc = TknFileReader.parse(tknFile)

    val parseFromTextMethod: PrivateMethod[IndexedSeq[Tkn]] = PrivateMethod[IndexedSeq[Tkn]]('parseFromText)
    val tkns: IndexedSeq[Tkn] = TknFileReader invokePrivate parseFromTextMethod(Source.fromFile(tknFile).getLines())
    val text: String = Doc.getTextByTkns(tkns)

    "Cached doc" should "be saved correctly" in {
        Cache.saveDocsCache(Seq(doc))
        new File(EventNuggetConfig.cacheDir.get + "/docs") should exist
    }

    it should "equals to non-cached doc" in {
        // Clear cache
        val setDocCacheMethod: PrivateMethod[Option[Map[String, Doc]]] = PrivateMethod[Option[Map[String, Doc]]]('setDocCache)
        Cache invokePrivate setDocCacheMethod(None)

        // Load cache
        Cache.loadDocsCache()

        Cache.getDocCache(text) shouldBe a [Some[_]]

        val docByCache: Doc = Cache.getDocCache(text).get
        doc.text shouldBe docByCache.text
    }
}
