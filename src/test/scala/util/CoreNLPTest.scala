package util

import _root_.util.nlp.CoreNLP
import edu.stanford.nlp.semgraph.SemanticGraph
import edu.stanford.nlp.trees.Tree
import org.scalatest.{FlatSpec, Matchers}

import scala.collection.JavaConversions._

/**
  * Created by nakayama.
  */
class CoreNLPTest extends FlatSpec with Matchers {
    val text =
        """
          |This is a test text.
          |
          |New line. Some line, but independent sentence.
          |<br/>
          |
          |$)?(~*"'\¢]-[`
          |
          |&amp;
          |
          |&lt;
          |&gt;
          |
          | """.stripMargin

    val coreNLP: CoreNLP = new CoreNLP(text)

    "CoreNLP" should "have expected parse trees" in {
        val parseTrees: Seq[Tree] = coreNLP.parseTrees
        parseTrees.length shouldBe 5

        val allTokenInParseTree = coreNLP.allTokenInParseTree
        allTokenInParseTree.length shouldBe 34

        val token0 = allTokenInParseTree.head
        token0.isLeaf shouldBe true
        token0.nodeString shouldBe "This"

        val token5 = allTokenInParseTree(5)
        token5.isLeaf shouldBe true
        token5.nodeString shouldBe "."

        val token10 = allTokenInParseTree(10)
        token10.isLeaf shouldBe true
        token10.nodeString shouldBe "line"

        val token16 = allTokenInParseTree(16)
        token16.isLeaf shouldBe true
        token16.nodeString shouldBe "<br/>"

        val token17 = allTokenInParseTree(17)
        token17.isLeaf shouldBe true
        token17.nodeString shouldBe """$"""

        val token18 = allTokenInParseTree(18)
        token18.isLeaf shouldBe true
        token18.nodeString shouldBe """)"""

        val token19 = allTokenInParseTree(19)
        token19.isLeaf shouldBe true
        token19.nodeString shouldBe """?"""

        val token20 = allTokenInParseTree(20)
        token20.isLeaf shouldBe true
        token20.nodeString shouldBe """("""

        val token21 = allTokenInParseTree(21)
        token21.isLeaf shouldBe true
        token21.nodeString shouldBe """~"""

        val token22 = allTokenInParseTree(22)
        token22.isLeaf shouldBe true
        token22.nodeString shouldBe """*"""

        val token23 = allTokenInParseTree(23)
        token23.isLeaf shouldBe true
        token23.nodeString shouldBe "\""

        val token24 = allTokenInParseTree(24)
        token24.isLeaf shouldBe true
        token24.nodeString shouldBe "'"

        val token25 = allTokenInParseTree(25)
        token25.isLeaf shouldBe true
        token25.nodeString shouldBe """\"""

        val token26 = allTokenInParseTree(26)
        token26.isLeaf shouldBe true
        token26.nodeString shouldBe """¢"""

        val token27 = allTokenInParseTree(27)
        token27.isLeaf shouldBe true
        token27.nodeString shouldBe """]"""

        val token28 = allTokenInParseTree(28)
        token28.isLeaf shouldBe true
        token28.nodeString shouldBe """-"""

        val token29 = allTokenInParseTree(29)
        token29.isLeaf shouldBe true
        token29.nodeString shouldBe """["""

        val token30 = allTokenInParseTree(30)
        token30.isLeaf shouldBe true
        token30.nodeString shouldBe """`"""

        val token31 = allTokenInParseTree(31)
        token31.isLeaf shouldBe true
        token31.nodeString shouldBe """&amp;"""

        val token32 = allTokenInParseTree(32)
        token32.isLeaf shouldBe true
        token32.nodeString shouldBe """<"""

        val token33 = allTokenInParseTree(33)
        token33.isLeaf shouldBe true
        token33.nodeString shouldBe """>"""
    }

    it should "have expected depparse trees" in {
        val depParseTrees: Seq[SemanticGraph] = coreNLP.depParseTrees
        depParseTrees.foreach(println)
        depParseTrees.length shouldBe 5

        val tree0 = depParseTrees.head
        val root0 = tree0.getFirstRoot
        root0.word() shouldBe "text"
        root0.index() shouldBe 5
        tree0.getChildren(root0).map(_.word()) should contain theSameElementsAs Seq("This", "is", "a", "test", ".")

        val tree2 = depParseTrees(2)
        val root2 = tree2.getFirstRoot
        root2.word() shouldBe "line"
        root2.index() shouldBe 2
        tree2.getChildren(root2).map(_.word()) should contain theSameElementsAs Seq("Some", ",", "but", "sentence", ".")
    }
}