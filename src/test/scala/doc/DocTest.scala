package doc

import java.io.File

import edu.stanford.nlp.trees.Tree
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}
import reader.TknFileReader

import scala.collection.immutable.IndexedSeq

/**
  * Created by nakayama.
  */
class DocTest extends FlatSpec with Matchers with PrivateMethodTester {
    val tknFile: File = new File("src/test/resources/event_nugget/news1.tab")
    val doc: Doc = TknFileReader.parse(tknFile)
    val tkns: Seq[Tkn] = doc.tkns

    "Parsed doc" should "have expected values" in {
        doc.text shouldBe {
            """<DOC id="AFP_ENG_20091008.0600" type="story" > <HEADLINE> ALERT ¥ < HEADLINE> <TEXT> <P> Twelve killed, 83 wounded in Kabul suicide attack: Interior Ministry this is te  st se  nte  nce < P> < TEXT> < DOC>"""
        }
    }

    it should "have expected tkns" in {
        val tkn0 = tkns.head
        tkn0.text shouldBe "<DOC id=\"AFP_ENG_20091008.0600\" type=\"story\" >"
        tkn0.prevTkn shouldBe None
        tkn0.nextTkn shouldBe Some(tkns(1))
        tkn0.nextTkn.get.text shouldBe "<HEADLINE>"
        val tkn1 = tkns(1)
        tkn1.text shouldBe "<HEADLINE>"
        tkn1.prevTkn shouldBe Some(tkn0)
        tkn1.prevTkn.get.text shouldBe "<DOC id=\"AFP_ENG_20091008.0600\" type=\"story\" >"
        tkn1.nextTkn shouldBe Some(tkns(2))
        tkn1.nextTkn.get.text shouldBe "ALERT"
        val tkn5 = tkns(5)
        tkn5.text shouldBe "HEADLINE>"
        val tkn21 = tkns(21)
        tkn21.text shouldBe "is"
        val tkn32 = tkns.last
        tkn32.text shouldBe "DOC>"
        tkn32.prevTkn shouldBe Some(tkns(31))
        tkn32.prevTkn.get.text shouldBe "<"
        tkn32.nextTkn shouldBe None
    }

    it should "have expected Stanford CoreNLP annotations" in {
        val tkn0 = tkns.head
        tkn0.pos shouldBe "NN"
        tkn0.pos2 shouldBe "NN"
        tkn0.ne shouldBe None
        tkn0.nodeInParseTree.get.nodeString() shouldBe """<DOC id="AFP_ENG_20091008.0600" type="story" >"""
        val tkn5 = tkns(5)
        tkn5.pos shouldBe "CD"
        tkn5.pos2 shouldBe "CD"
        tkn5.ne shouldBe Some("NUMBER")
        tkn5.nodeInParseTree.get.nodeString() shouldBe """HEADLINE"""
        val tkn21 = tkns(21)
        tkn21.pos shouldBe "VBZ"
        tkn21.pos2 shouldBe "VB"
        tkn21.nodeInParseTree.get.nodeString() shouldBe """is"""
        val tkn32 = tkns.last
        tkn32.pos shouldBe "NN"
        tkn32.pos2 shouldBe "NN"
        tkn32.nodeInParseTree.get.nodeString() shouldBe """DOC"""
    }

    it should "have expected parse tree" in {
        val tokensInParseTree: Seq[Tree] = doc.allTokenInParseTree
        tokensInParseTree.length shouldBe 37

        val t0 = tokensInParseTree.head
        t0.isLeaf shouldBe true
        t0.nodeString shouldBe """<DOC id="AFP_ENG_20091008.0600" type="story" >"""

        val t1 = tokensInParseTree(1)
        t1.isLeaf shouldBe true
        t1.nodeString shouldBe """<HEADLINE>"""

        val t5 = tokensInParseTree(5)
        t5.isLeaf shouldBe true
        t5.nodeString shouldBe """HEADLINE"""

        val t6 = tokensInParseTree(6)
        t6.isLeaf shouldBe true
        t6.nodeString shouldBe """>"""

        val t32 = tokensInParseTree(32)
        t32.isLeaf shouldBe true
        t32.nodeString shouldBe """TEXT"""

        doc.parseTrees.foreach(t => println(t.pennString()))

        val roots: Seq[Tree] = doc.parseTrees.map(_.preOrderNodeList().get(0))

        def getSentenceRoot(node: Tree, roots: Seq[Tree]): Tree =
            roots.find(node.parent(_) != null) match {
                case Some(t) => t
                case _ => throw new RuntimeException("Root node in parse tree was not found: " + node)
            }

        var node = doc.allTokenInParseTree(10)
        val root: Tree = getSentenceRoot(node, roots)

//        def searchMainVerb(nodes: Seq[Tree]) = {
//            nodes.flatMap(_.children()).find(_.nodeString().startsWith("VB [")) match {
//                case Some(n) => n
//                case _ => searchMainVerb(nodes)
//            }
//        }

    }

    it should "have expected correspondent with tkns" in {
        val irregularTknLines =
            """
              |1	<doc id="bolt	0	13
              |2	eng	14	17
              |3	DF	18	20
              |4	199	21	24
              |5	192821	25	31
              |6	6708363">	32	41
              |7	"abc	42	46
              |8	d"ef	46	50
              |9	gh	51	53
              |10	i)	53	55
              |11	etc.	56	60
              |12	but	61	64
              |13	etc.	65	69
              |14	I	70	71
              |15	U.S	72	75
              |16	&lt;	76	80
              |17	.	80	81
              |18	&gt;	81	85
              |19	?	85	98
              |""".stripMargin.lines

        val parseFromTextMethod: PrivateMethod[IndexedSeq[Tkn]] = PrivateMethod[IndexedSeq[Tkn]]('parseFromText)
        val irregularTkns: IndexedSeq[Tkn] = TknFileReader invokePrivate parseFromTextMethod(irregularTknLines)
        val irregularDoc: Doc = new Doc("irregular.tkn", irregularTkns)

        val tokensInParseTree: Seq[Tree] = irregularDoc.allTokenInParseTree
        tokensInParseTree.length shouldBe 17

        val node0 = tokensInParseTree.head
        node0.nodeString() shouldBe """<doc id="bolt eng DF 199 192821 6708363">"""
        val node1 = tokensInParseTree(1)
        node1.nodeString() shouldBe "\""
        val node2 = tokensInParseTree(2)
        node2.nodeString() shouldBe """abcd"""
        val node5 = tokensInParseTree(5)
        node5.nodeString() shouldBe """ghi"""

        val t0 = irregularDoc.tkns.head
        t0.text shouldBe """<doc id="bolt"""
        t0.nodeInParseTree.get.nodeString() shouldBe """<doc id="bolt eng DF 199 192821 6708363">"""

        val t1 = irregularDoc.tkns(1)
        t1.text shouldBe """eng"""
        t1.nodeInParseTree.get.nodeString() shouldBe """<doc id="bolt eng DF 199 192821 6708363">"""

        val t6 = irregularDoc.tkns(6)
        t6.text shouldBe """"abc"""
        t6.nodeInParseTree.get.nodeString() shouldBe "\""

        val t7 = irregularDoc.tkns(7)
        t7.text shouldBe """d"ef"""
        t7.nodeInParseTree.get.nodeString() shouldBe """abcd"""

        val t8 = irregularDoc.tkns(8)
        t8.text shouldBe """gh"""
        t8.nodeInParseTree.get.nodeString() shouldBe """ghi"""

        val t9 = irregularDoc.tkns(9)
        t9.text shouldBe """i)"""
        t9.nodeInParseTree.get.nodeString() shouldBe """ghi"""

        val t10 = irregularDoc.tkns(10)
        t10.text shouldBe """etc."""
        t10.nodeInParseTree.get.nodeString() shouldBe """etc."""

        val t11 = irregularDoc.tkns(11)
        t11.text shouldBe """but"""
        t11.nodeInParseTree.get.nodeString() shouldBe """but"""

        val t12 = irregularDoc.tkns(12)
        t12.text shouldBe """etc."""
        t12.nodeInParseTree.get.nodeString() shouldBe """etc."""

        val t13 = irregularDoc.tkns(13)
        t13.text shouldBe """I"""
        t13.nodeInParseTree.get.nodeString() shouldBe """I"""

        val t14 = irregularDoc.tkns(14)
        t14.text shouldBe """U.S"""
        t14.nodeInParseTree.get.nodeString() shouldBe """U.S"""

        val t15 = irregularDoc.tkns(15)
        t15.text shouldBe """&lt;"""
        t15.nodeInParseTree.get.nodeString() shouldBe """<"""

        val t16 = irregularDoc.tkns(16)
        t16.text shouldBe """."""
        t16.nodeInParseTree.get.nodeString() shouldBe """."""

        val t17 = irregularDoc.tkns(17)
        t17.text shouldBe """&gt;"""
        t17.nodeInParseTree.get.nodeString() shouldBe """>"""

        val t18 = irregularDoc.tkns(18)
        t18.text shouldBe """?"""
        t18.nodeInParseTree.get.nodeString() shouldBe """?"""
    }
}
