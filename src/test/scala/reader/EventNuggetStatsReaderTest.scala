package reader

import java.io.File

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by nakayama.
  */
class EventNuggetStatsReaderTest extends FlatSpec with Matchers {
    val fileName2Genre = EventNuggetStatsReader.eventNuggetStatsFileParser(
        new File("src/test/resources/event_nugget/event_nugget_stats.tab"))

    "The mapping of file name to genre" should "" in {
        fileName2Genre.get("mlt2") should equal {
            Some("multi_post")
        }

        fileName2Genre.get("news2") should equal {
            Some("newswire")
        }
    }
}