package reader

import java.io.File

import doc.{Doc, Tkn}
import org.scalatest.{FlatSpec, Matchers, PrivateMethodTester}

import scala.collection.immutable.IndexedSeq
import scala.io.Source

/**
  * Created by nakayama.
  */
class TknFileReaderTest extends FlatSpec with Matchers with PrivateMethodTester {
    val tknFilePath = "src/test/resources/event_nugget/news1.tab"

    val tknText: String = Source.fromFile(tknFilePath).getLines().mkString("\n")
    val parseFromTextMethod: PrivateMethod[IndexedSeq[Tkn]] = PrivateMethod[IndexedSeq[Tkn]]('parseFromText)
    val tkns: IndexedSeq[Tkn] = TknFileReader invokePrivate parseFromTextMethod(tknText.lines)

    "Parsed data" should "have expected number of data" in {
        tkns.length shouldBe 33
    }

    it should "have expected data" in {
        val tkn2 = tkns(2)
        tkn2.text shouldBe "ALERT"
        tkn2.startOffset shouldBe 58
        tkn2.endOffset shouldBe 63
        val tkn23 = tkns(23)
        tkn23.text shouldBe "st"
        tkn23.startOffset shouldBe 170
        tkn23.endOffset shouldBe 172
    }

    val doc: Doc = TknFileReader.parse(new File(tknFilePath))

    "doc" should "be parsed doc" in {
        doc.tkns shouldBe tkns
    }
}