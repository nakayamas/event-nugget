package event

import doc.Doc

/**
  * Created by nakayama.
  */
final case class EventHopperDoc(override val doc: Doc,
                                override val sourceType: Symbol,
                                eventHoppers: Seq[EventHopper]
                               ) extends EventDoc(doc, sourceType) {
    val eventNuggets: Seq[EventNugget] = eventHoppers.flatMap(_.eventNuggets)
    eventNuggets.foreach(_.eventDoc = this)
    eventHoppers.foreach(_.eventDoc = this)
    doc.eventHopperDoc = Some(this)
}
