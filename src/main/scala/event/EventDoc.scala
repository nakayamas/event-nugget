package event

import doc.Doc

/**
  * Created by nakayama.
  */
abstract class EventDoc(val doc: Doc,
                        val sourceType: Symbol = 'unknown
                       ) extends Serializable {
    val id: String = doc.name match {
        case s if s.endsWith(".tab") => s.dropRight(4)
        case s => s
    }

    val eventNuggets: Seq[EventNugget]
}