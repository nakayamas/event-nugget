package event

/**
  * Created by nakayama.
  */
class PredictedEventHopper(id: String,
                           var predictedEventNuggetList: Seq[EventNugget])
    extends EventHopper(id, Nil)
