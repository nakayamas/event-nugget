package event

/**
  * Created by nakayama.
  */
case class EventHopper(id: String,
                       eventNuggets: Seq[EventNugget]
                      ) {
    val tbfId = id.replace("h-", "C")
    eventNuggets.foreach(_.eventHopper = Some(this))

    private var _eventDoc: EventDoc = null

    def eventDoc: EventDoc = _eventDoc

    def eventDoc_=(eventDoc: EventDoc) = _eventDoc = eventDoc
}
