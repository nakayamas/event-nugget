package event

import doc.Doc

/**
  * Created by nakayama.
  */
final case class EventNuggetDoc(override val doc: Doc,
                                override val sourceType: Symbol,
                                eventNuggets: Seq[EventNugget])
    extends EventDoc(doc, sourceType) {
    eventNuggets.foreach(_.eventDoc = this)
    doc.eventNuggetDoc = Some(this)
}
