package event

/**
  * Created by nakayama.
  */
class PredictedEventNugget(eventNugget: EventNugget,
                           var predictedRealis: Option[Realis.Value] = None)
    extends EventNugget(
        eventNugget.id,
        eventNugget.eventType,
        eventNugget.eventSubtype,
        eventNugget.realis,
        eventNugget.formality,
        eventNugget.schedule,
        eventNugget.medium,
        eventNugget.audience,
        eventNugget.startOffset,
        eventNugget.length,
        eventNugget.text
    )
