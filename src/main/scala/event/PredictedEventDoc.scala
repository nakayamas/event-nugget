package event

import doc.Doc

/**
  * Created by nakayama.
  */
case class PredictedEventDoc(override val doc: Doc,
                             var predictedEventNuggets: Seq[PredictedEventNugget] = Seq(),
                             var predictedEventHoppers: Seq[PredictedEventHopper] = Seq())
    extends EventDoc(doc) {
    override val eventNuggets: Seq[EventNugget] = doc.eventNuggets
    val eventDoc = eventNuggets.headOption match {
        case Some(en) => en.eventDoc
        case _ => throw new RuntimeException("Event doc was not found: " + doc.name)
    }
    predictedEventNuggets.foreach(_.eventDoc = eventDoc)
}
