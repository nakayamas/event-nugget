package event

import doc.Tkn
import util.PrettyString

/**
  * Created by nakayama.
  */
case class EventNugget(
                          id: String,
                          eventType: Symbol,
                          eventSubtype: Symbol,
                          realis: Realis.Value,
                          formality: Symbol,
                          schedule: Symbol,
                          medium: Symbol,
                          audience: Symbol,
                          startOffset: Int,
                          length: Int,
                          text: String
                      ) {
    val words: Array[String] = text.split(" ")

    val endOffset: Int = startOffset + length

    val startOffsets: Array[Int] = {
        var offset = startOffset
        words.map { w =>
            val currentOffset = offset
            offset += w.length + 1 // word length + space
            currentOffset
        }
    }

    private var _eventHopper: Option[EventHopper] = None

    def eventHopper: Option[EventHopper] = _eventHopper

    def eventHopper_=(eventHopper: Option[EventHopper]) = _eventHopper = eventHopper

    private var _eventDoc: EventDoc = _

    def eventDoc: EventDoc = _eventDoc

    def eventDoc_=(eventDoc: EventDoc) = _eventDoc = eventDoc

    lazy val headTkn: Option[Tkn] = eventDoc.doc.tkns.find(startOffset <= _.startOffset)

    lazy val tknIndices: Array[Int] = startOffsets.map(sos => eventDoc.doc.tkns.find(_.startOffset == sos).get.tknIndex)

    val tbfId: String = id.replace("em-", "E")

    val eventTypeExp: String =
        PrettyString.upperFirstLowerRest(eventType.name) + "_" + PrettyString.upperFirstLowerRest(eventSubtype.name)

    val realisExp: String =
        PrettyString.upperFirstLowerRest(realis.toString)
}