package coreference

import java.io.File

import config.EventNuggetConfig
import de.bwaldvogel.liblinear._
import doc.Doc
import event._
import org.apache.commons.io.FileUtils
import reader.TknFilesReader
import scopt.OptionParser
import util.ml.liblinear.{LiblinearFormatter, LiblinearMapper, Predictor}
import util.tree.Tree
import util.{Cache, FileOps}

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.sys.process.Process

/**
  * Created by nakayama.
  */
object CoreferenceResolver {
    def main(args: Array[String]): Unit = {
        val options: ArgOption = argParser(args)

        // Parse args
        if (options.config != null) {
            EventNuggetConfig.set(options.config)
        }

        val tknDirForTraining: File = EventNuggetConfig.tknDirForCoreferenceTraining match {
            case Some(d) => new File(d)
            case _ => throw new RuntimeException("Tkn directory setting of training data was not found.")
        }
        val tknFilesForTraining: Array[File] = FileOps.getFilesFromDir(tknDirForTraining)
        val eventHopperFilesForTraining: Array[File] =
            FileOps.getFilesFromDir(EventNuggetConfig.eventHopperDirForCoreferenceTraining match {
                case Some(d) => d
                case _ => throw new RuntimeException("Event Hopper directory setting of training data was not found.")
            })
        val outputDir: File = FileOps.withMakeParentDirs(
            if (options.outputDir == null) {
                EventNuggetConfig.outputDir match {
                    case Some(d) if d != "" => new File(d)
                    case _ => throw new RuntimeException("Output directory setting was not found.")
                }
            } else options.outputDir)

        if (options.paramTune) {
            val eventNuggetFilesForPrediction: Array[File] =
                FileOps.getFilesFromDir(EventNuggetConfig.eventNuggetDirForCoreferenceTraining match {
                    case Some(d) => d
                    case _ => throw new RuntimeException("Event Nugget directory setting of training data was not found.")
                })
            val sourceDirForTraining: File = EventNuggetConfig.sourceDirForCoreferenceTraining match {
                case Some(d) => new File(d)
                case _ => throw new RuntimeException("Tkn directory setting of training data was not found.")
            }
            val goldTbfPath: String = EventNuggetConfig.goldTbfForCoreferenceTraining match {
                case Some(f) => f
                case _ => throw new RuntimeException("gold.tbf setting of training data was not found.")
            }

            parameterTuning(numberOfFolds = 5,
                tknFilesForTraining,
                eventHopperFilesForTraining,
                eventNuggetFilesForPrediction,
                costs = Seq(0.01), // Seq(0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 10.0)
                biases = Seq(-1), // Seq(-1, 0, 1)
                tknDirForTraining, sourceDirForTraining, goldTbfPath,
                outputDir)
        } else {
            val tknDirForPrediction: File = EventNuggetConfig.tknDirForCoreferencePrediction match {
                case Some(d) => new File(d)
                case _ => throw new RuntimeException("Tkn directory setting of test data was not found.")
            }
            val tknFilesForPrediction: Array[File] = FileOps.getFilesFromDir(tknDirForPrediction)
            val eventNuggetFilesForPrediction: Array[File] =
                FileOps.getFilesFromDir(EventNuggetConfig.eventNuggetDirForCoreferencePrediction match {
                    case Some(d) => d
                    case _ => throw new RuntimeException("Event Nugget directory setting of test data was not found.")
                })
            val cost: Double =
                options.params.getOrElse("cost", EventNuggetConfig.realisTrainCost match {
                    case Some(x) => x
                    case _ => throw new RuntimeException("Training cost setting of Realis was not found.")
                })
            val bias: Double =
                options.params.getOrElse("bias", EventNuggetConfig.realisTrainBias match {
                    case Some(x) => x
                    case _ => throw new RuntimeException("Training bias setting of Realis was not found.")
                })
            val goldTbfPath: String = EventNuggetConfig.goldTbfForCoreferencePrediction match {
                case Some(f) => f
                case _ => throw new RuntimeException("gold.tbf setting of test data was not found.")
            }

            mainProcess(tknFilesForTraining, eventHopperFilesForTraining,
                tknFilesForPrediction, eventNuggetFilesForPrediction,
                cost, bias,
                outputDir)
            evaluate(tknDirForPrediction.getAbsolutePath, goldTbfPath, outputDir)
        }
    }

    final def parameterTuning(numberOfFolds: Int,
                              allTknFiles: Array[File],
                              eventHopperFiles: Array[File],
                              eventNuggetFiles: Array[File],
                              costs: Seq[Double], biases: Seq[Double],
                              tknDirForTraining: File, sourceDirForTraining: File, goldTbfPath: String,
                              outputDir: File) = {
        val nFoldedTknFiles: Map[Symbol, Array[File]] =
            allTknFiles.zipWithIndex.groupBy {
                case (tfs, i) => Symbol(s"n${i % numberOfFolds}")
            }.map {
                case (nFold, tknFilesWithIndex) => nFold -> tknFilesWithIndex.map(_._1)
            }
        for {
            cost <- costs
            bias <- biases
            (nFold, tknFilesForPrediction) <- nFoldedTknFiles
        } {
            val tknFilesForTraining: Array[File] = nFoldedTknFiles.collect {
                case (n, tfs) if n != nFold => tfs
            }.flatten.toArray

            // Output directory
            val nFoldOutputDir: File = FileOps.withMakeParentDirs(
                s"${outputDir.getAbsolutePath}/c${cost}_b${bias}_${nFold.name}")

            mainProcess(tknFilesForTraining, eventHopperFiles,
                tknFilesForPrediction, eventNuggetFiles,
                cost, bias,
                nFoldOutputDir)
            evaluate(tknDirForTraining.getAbsolutePath, goldTbfPath, nFoldOutputDir)
            visualize(tknDirForTraining, sourceDirForTraining, nFoldOutputDir)
        }
    }

    final def mainProcess(tknFilesForTraining: Array[File], eventHopperFilesForTraining: Array[File],
                          tknFilesForPrediction: Array[File], eventNuggetFilesForPrediction: Array[File],
                          cost: Double, bias: Double,
                          outputDir: File): Unit = {
        // Parse files
        val allTrainDoc: Array[Doc] =
            new TknFilesReader(tknFilesForTraining).parseWithEventHopperFiles(eventHopperFilesForTraining)
        val allPredictDoc: Array[Doc] =
            new TknFilesReader(tknFilesForPrediction).parseWithEventNuggetFiles(eventNuggetFilesForPrediction)
        if (EventNuggetConfig.cacheSave)
            Cache.saveDocsCache(allTrainDoc ++ allPredictDoc)

        train(allTrainDoc, cost, bias, outputDir)
        val predictedDocs: Seq[PredictedEventDoc] = predict(allPredictDoc, outputDir)

        // Output as tbf
        val tbf: String = tbfFormatter(predictedDocs)
        FileOps.write(new File(s"${outputDir.getAbsolutePath}/system.tbf")) {
            _.print(tbf)
        }
    }

    final def train(docs: Array[Doc], cost: Double, bias: Double, outputDir: File): Unit = {
        // Prepare model and feature files
        val modelFile: File = new File(EventNuggetConfig.coreferenceModelFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Model file setting of Event Nugget Coreference was not found.")
        })
        val liblinearFeatureFile: File = new File(EventNuggetConfig.coreferenceLiblinearFeatureFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Liblinear feature file setting of Event Nugget Coreference was not found.")
        })
        val classLabelIndexFile: File = new File(EventNuggetConfig.coreferenceClassLabelIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Class label index file setting of Event Nugget Coreference was not found.")
        })
        val featureNameIndexFile: File = new File(EventNuggetConfig.coreferenceFeatureNameIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Feature name index file setting of Event Nugget Coreference was not found.")
        })

        // Extract feature
        val featureExtractor: CoreferenceFeatureExtractor = new CoreferenceFeatureExtractor('train)
        featureExtractor.createFeaturesForTrain(docs.flatMap(_.eventHopperDoc))

        // Map feature name to number (Liblinear format)
        val liblinearMapper: LiblinearMapper = LiblinearMapper(featureExtractor.samples)
        LiblinearFormatter(featureExtractor.samples, liblinearMapper).saveAsLiblinearTrainFormat(liblinearFeatureFile)
        liblinearMapper.saveClassLabelIndex(classLabelIndexFile)
        liblinearMapper.saveFeatureNameIndex(featureNameIndexFile)

        // Train with LibLinear
        val parameter = new Parameter(SolverType.L2R_LR_DUAL, cost, Double.PositiveInfinity)
        val problem = Problem.readFromFile(liblinearFeatureFile, bias)
        val model = Linear.train(problem, parameter)
        model.save(modelFile)
    }

    final def predict(docs: Array[Doc], modelAndFeatureDir: File): Array[PredictedEventDoc] = {
        // Read model and feature files
        val modelFile: File = new File(EventNuggetConfig.coreferenceModelFile match {
            case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Model file setting of Event Nugget Coreference was not found.")
        })
        val classLabelIndexFile: File = new File(EventNuggetConfig.coreferenceClassLabelIndexFile match {
            case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Class label index file setting of Event Nugget Coreference was not found.")
        })
        val featureNameIndexFile: File = new File(EventNuggetConfig.coreferenceFeatureNameIndexFile match {
            case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Feature name index file setting of Event Nugget Coreference was not found.")
        })

        // Load model and features
        val model: Model = Model.load(modelFile)
        val liblinearMapper = LiblinearMapper(classLabelIndexFile, featureNameIndexFile)
        val predictor = new Predictor(model, liblinearMapper)
        val extractor = new CoreferenceFeatureExtractor('predict)
        for {
            doc <- docs
            eventNuggetDoc <- doc.eventNuggetDoc
        } yield {
            // predict and create a tree
            val tree = new Tree
            val eventNuggets = eventNuggetDoc.eventNuggets
            eventNuggets.indices.foreach { n =>
                val i: EventNugget = eventNuggets(n)
                var mostProbableEventNugget: (EventNugget, Double) =
                    (null, predictor.predictProbability(extractor.extractFeatures(i).toMap))
                for {
                    j: EventNugget <- eventNuggets.take(n) if i.realis == j.realis && i.eventSubtype == j.eventSubtype
                } {
                    val probability = predictor.predictProbability(extractor.extractFeatures(i, j).toMap)
                    if (mostProbableEventNugget._2 < probability) {
                        mostProbableEventNugget = (j, probability)
                    }
                }
                if (mostProbableEventNugget._1 == null) {
                    // create a new cluster
                    tree.add(i)
                } else {
                    // append to an exist node
                    tree.nodes.find(_.eventNugget == mostProbableEventNugget._1).get.add(i)
                }
            }

            // convert a tree to clusters
            val eventHopperId: Iterator[Int] = Stream.from(1).iterator
            new PredictedEventDoc(doc, predictedEventHoppers =
                // create clusters
                tree.nodes.filter(_.isClusterTop).map(_.subNodes.map(_.eventNugget)).map {
                    new PredictedEventHopper("h-" + eventHopperId.next.toString, _)
                })
        }
    }

    final def evaluate(tknDirPath: String, goldTbfPath: String, outputDir: File) = {
        val outputDirPath: String = outputDir.getAbsolutePath
        val evaluateCommand: Seq[String] =
            EventNuggetConfig.evaluateScript match {
                case Some(s) =>
                    Seq("python", s,
                        "-g", goldTbfPath,
                        "-s", s"${outputDirPath}/system.tbf",
                        "-d", s"${outputDirPath}/comparison",
                        "-t", tknDirPath,
                        "-c", s"${outputDirPath}/conll-coref")
                case _ => throw new RuntimeException("evaluation script setting was not found.")
            }
        println("cmd: " + evaluateCommand)
        FileOps.write(new File(s"${outputDirPath}/result")) {
            val result = Process(evaluateCommand).lineStream_!.mkString("\n")
            println(result)
            _.println(result)
        }
    }

    final def tbfFormatter(predictedEventDocs: Seq[PredictedEventDoc]): String = {
        val tbf = new StringBuilder()
        for (predictedEventDoc <- predictedEventDocs) {
            tbf.append("#BeginOfDocument " + predictedEventDoc.id + "\n")
            for (predictedHopper <- predictedEventDoc.predictedEventHoppers) {
                val singleTaggedEventNuggets: ArrayBuffer[EventNugget] = ArrayBuffer()
                for (predictedEventNugget <- predictedHopper.predictedEventNuggetList) {
                    // Skip double tagged event nugget
                    singleTaggedEventNuggets.find(_.startOffset == predictedEventNugget.startOffset) match {
                        case Some(x) => // double tagged event nugget
                        case _ => {
                            // event nuggets
                            tbf.append(Seq("system1", predictedEventDoc.id,
                                predictedEventNugget.tbfId, predictedEventNugget.tknIndices.mkString(","), predictedEventNugget.text,
                                predictedEventNugget.eventTypeExp, predictedEventNugget.realisExp).mkString("\t"))
                            tbf.append("\n")
                            singleTaggedEventNuggets += predictedEventNugget
                        }
                    }
                }
                // coreferences
                tbf.append(Seq("@Coreference", predictedHopper.tbfId,
                    singleTaggedEventNuggets.map(_.tbfId).mkString(",")).mkString("\t"))
                tbf.append("\n")
            }
            tbf.append("#EndOfDocument\n")
        }
        tbf.result()
    }

    final def visualize(tknDir: File, sourceDir: File, result_dir: File) {
        // cross document で system が document を吐かない場合に comparison が - となるので、それを削除する
        val comparisonFilePath = result_dir + "/comparison"
        val comparison = new StringBuilder
        var exist_system_output: Boolean = true
        val document = new StringBuilder
        Source.fromFile(comparisonFilePath).getLines.foreach { l =>
            var fixedLine: String = l + "\n"
            // unknown bug of scorer_v1.6.py
            fixedLine = fixedLine.replace(",,", ",")
            fixedLine = fixedLine.replace(",\n", "\n")

            document.append(fixedLine)
            if (fixedLine.endsWith("|\t-\t-\n")) {
                exist_system_output = false
            } else if (fixedLine.startsWith("#EndOfDocument")) {
                if (exist_system_output) comparison.append(document.result())
                document.setLength(0)
                exist_system_output = true
            }
        }
        val fixedComparisonFilePath = comparisonFilePath + "_fixed"
        FileOps.writeWithMakeParentDirs(new File(fixedComparisonFilePath)) {
            _.write(comparison.result())
        }

        val visualization_dir = new File(result_dir + "/visualization")
        if (!visualization_dir.exists()) {
            val visualizationTemplateDir: File = new File(EventNuggetConfig.visualizationTemplate match {
                case Some(d) => d
                case _ => throw new RuntimeException("visualization template setting was not found")
            })
            FileUtils.copyDirectory(visualizationTemplateDir, visualization_dir)
        }

        val visualizeCommand: Seq[String] =
            (for {
                script <- EventNuggetConfig.visualizerScript
                    .toRight(throw new RuntimeException("visualizer script setting was not found.")).right
                goldTbf <- EventNuggetConfig.goldTbfForCoreferencePrediction
                    .toRight(throw new RuntimeException("gold.tbf setting was not found.")).right
            } yield {
                Seq("python", script,
                    "-d", fixedComparisonFilePath,
                    "-t", tknDir.getAbsolutePath,
                    "-x", sourceDir.getAbsolutePath,
                    "-v", visualization_dir.getAbsolutePath)
            }).merge
        println("cmd: " + visualizeCommand)
        FileOps.write(new File(s"${result_dir.getAbsolutePath}/visualization_result")) {
            val result = Process(visualizeCommand).lineStream_!.mkString("\n")
            println(result)
            _.println(result)
        }
    }

    final private[this] case class ArgOption(config: File = null,
                                             paramTune: Boolean = false,
                                             params: Map[String, Double] = Map(),
                                             outputDir: File = null)

    final private[this] def argParser(args: Array[String]): ArgOption =
        new OptionParser[ArgOption](this.getClass.getName) {
            opt[File]('c', "config")
                .action((x, c) => c.copy(config = x))
                .validate { x =>
                    if (!x.exists) {
                        failure("configuration file was not found: " + x.getAbsolutePath)
                    } else success
                }
                .text("configuration file")

            opt[Unit]("param-tune")
                .action((_, c) => c.copy(paramTune = true))
                .text("parameter tune mode or not")

            opt[Map[String, Double]]("params").optional
                .valueName("cost=value,bias=value")
                .action((x, c) => c.copy(params = x))
                .validate(x =>
                    x.keysIterator.find(k => k != "cost" && k != "bias") match {
                        case Some(k) => failure("unknown parameter name: " + k)
                        case _ => success
                    })
                .text("svm parameters")

            opt[File]('o', "output-dir").required()
                .valueName("<dir>")
                .action((x, c) => c.copy(outputDir = x))
                .validate { x =>
                    if (x.exists && x.isFile) {
                        failure(s"output-dir must be a directory. ${x.getAbsolutePath} is a file")
                    } else success
                }
                .text("output directory")
        }.parse(args, ArgOption()) match {
            case Some(x) => x
            case _ => System.exit(-1); null
        }
}
