package coreference

import event.{EventHopperDoc, EventNugget}
import util.ml.liblinear.LiblinearInstance

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
final class CoreferenceFeatureExtractor(mode: Symbol = 'predict) {

    val samples: ArrayBuffer[LiblinearInstance] = ArrayBuffer[LiblinearInstance]()

    def createFeaturesForTrain(eventHopperDocs: Array[EventHopperDoc]): Unit =
        eventHopperDocs.foreach(createFeaturesForTrain)

    def createFeaturesForTrain(eventHopperDoc: EventHopperDoc): Unit =
        createFeaturesForTrain(eventHopperDoc.eventNuggets)

    def createFeaturesForTrain(eventNuggets: Seq[EventNugget]): Unit =
        eventNuggets.indices.foreach { n =>
            val i: EventNugget = eventNuggets(n)
            val corefer =
                if (i == i.eventHopper.get.eventNuggets.head) "corefer"
                else "non-corefer"
            samples += new LiblinearInstance(corefer, extractFeatures(i))
            for {
                j <- eventNuggets.take(n) if i.realis == j.realis
            } {
                val corefer =
                    if (i.eventHopper == j.eventHopper) "corefer"
                    else "non-corefer"
                samples += new LiblinearInstance(corefer, extractFeatures(i, j))
            }
        }

    /**
      * whether event nugget creates a new cluster
      *
      * @param i focused event nugget (mention)
      * @return
      */
    def extractFeatures(i: EventNugget): mutable.Map[String, Double] = {
        val features = mutable.Map.empty[String, Double]

        features.put("unseen_word_surface", featureValueConverter(
            i.eventDoc.eventNuggets.takeWhile(_ != i).exists(_.text == i.text)))
        features.put("surface_" + i.text.replace(" ", "_"), featureValueConverter(true))

        features
    }

    def extractFeatures(i: EventNugget, j: EventNugget) = {
        val features = mutable.Map.empty[String, Double]

        features.put("same_surface", featureValueConverter(
            i.text == j.text))
        // tkn file と stanford corenlp の tokenizer の違いで lemma や pos などが null の場合がある
        //        addFeature(features, "same_lemma", i.tkn.get.lemma != null && i.tkn.get.lemma == j.tkn.get.lemma)
//        val poslist = Seq(
//            i.headTkn match {
//                case Some(t) if t.pos != null => t.pos
//                case _ => "none"
//            },
//            j.headTkn match {
//                case Some(t) if t.pos != null => t.pos
//                case _ => "none"
//            })
//        features.put("pos_" + (poslist.sorted.mkString("_")), 1)

//        features.put("realis_" + Seq(i.realis.toString, j.realis.toString).sorted.mkString("_"), 1)
//        features.put("subtype_" + Seq(i.realis.toString, j.realis.toString).mkString("_"), 1)

        features
    }

    private[this] def featureValueConverter(v: Any): Double =
        v match {
            case i: Int => i
            case d: Double => d
            case b: Boolean => if (b) 1 else 0
            case null => 0.0
        }

}
