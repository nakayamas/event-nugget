package config

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._

/**
  * Created by nakayama.
  */
object EventNuggetConfig {
    final private[this] var config: Config = ConfigFactory.load()

    final def set(configFile: File): Unit =
        config = ConfigFactory.load(ConfigFactory.parseFile(configFile))

    // Event Nugget Detection config
    final private[this] lazy val detectionConfig: Option[Config] = config.as[Option[Config]]("event-nugget-detection")

    // train config
    final private[this] lazy val detectionTrainConfig: Option[Config] =
        detectionConfig match {
            case Some(c) => c.as[Option[Config]]("train")
            case _ => None
        }

    final lazy val eventNuggetDirForDetectionTraining: Option[String] =
        detectionTrainConfig match {
            case Some(c) => c.as[Option[String]]("event-nugget-dir")
            case _ => None
        }

    final lazy val tknDirForDetectionTraining: Option[String] =
        detectionTrainConfig match {
            case Some(c) => c.as[Option[String]]("tkn-dir")
            case _ => None
        }

    // predict config
    final private[this] lazy val detectionPredictConfig: Option[Config] =
        detectionConfig match {
            case Some(c) => c.as[Option[Config]]("predict")
            case _ => None
        }

    final lazy val eventNuggetDirForDetectionPrediction: Option[String] =
        detectionPredictConfig match {
            case Some(c) => c.as[Option[String]]("event-nugget-dir")
            case _ => None
        }

    final lazy val tknDirForDetectionPrediction: Option[String] =
        detectionPredictConfig match {
            case Some(c) => c.as[Option[String]]("tkn-dir")
            case _ => None
        }

    // realis config
    final private[this] lazy val realisConfig: Option[Config] = config.as[Option[Config]]("realis")

    final lazy val realisTrainCost: Option[Double] =
        realisConfig match {
            case Some(c) => c.as[Option[Double]]("train-cost")
            case _ => None
        }
    final lazy val realisTrainBias: Option[Double] =
        realisConfig match {
            case Some(c) => c.as[Option[Double]]("train-bias")
            case _ => None
        }
    final lazy val realisModelFile: Option[String] =
        realisConfig match {
            case Some(c) => c.as[Option[String]]("model-file")
            case _ => None
        }
    final lazy val realisLiblinearFeatureFile: Option[String] =
        realisConfig match {
            case Some(c) => c.as[Option[String]]("feature-file")
            case _ => None
        }
    final lazy val realisClassLabelIndexFile: Option[String] =
        realisConfig match {
            case Some(c) => c.as[Option[String]]("class-label-index-file")
            case _ => None
        }
    final lazy val realisFeatureNameIndexFile: Option[String] =
        realisConfig match {
            case Some(c) => c.as[Option[String]]("feature-name-index-file")
            case _ => None
        }

    // Event Nugget Coreference config
    final private[this] lazy val coreferenceConfig: Option[Config] = config.as[Option[Config]]("event-nugget-coreference")

    // train config
    final private[this] lazy val coreferenceTrainConfig: Option[Config] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[Config]]("train")
            case _ => None
        }

    final lazy val eventHopperDirForCoreferenceTraining: Option[String] =
        coreferenceTrainConfig match {
            case Some(c) => c.as[Option[String]]("event-hopper-dir")
            case _ => None
        }

    final lazy val eventNuggetDirForCoreferenceTraining: Option[String] =
        coreferenceTrainConfig match {
            case Some(c) => c.as[Option[String]]("event-nugget-dir")
            case _ => None
        }

    final lazy val tknDirForCoreferenceTraining: Option[String] =
        coreferenceTrainConfig match {
            case Some(c) => c.as[Option[String]]("tkn-dir")
            case _ => None
        }

    final lazy val sourceDirForCoreferenceTraining: Option[String] =
        coreferenceTrainConfig match {
            case Some(c) => c.as[Option[String]]("src-dir")
            case _ => None
        }

    final lazy val goldTbfForCoreferenceTraining: Option[String] =
        coreferenceTrainConfig match {
            case Some(c) => c.as[Option[String]]("gold-tbf")
            case _ => None
        }

    // predict config
    final private[this] lazy val coreferencePredictConfig: Option[Config] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[Config]]("predict")
            case _ => None
        }

    final lazy val eventNuggetDirForCoreferencePrediction: Option[String] =
        coreferencePredictConfig match {
            case Some(c) => c.as[Option[String]]("event-nugget-dir")
            case _ => None
        }

    final lazy val eventHopperDirForCoreferencePrediction: Option[String] =
        coreferencePredictConfig match {
            case Some(c) => c.as[Option[String]]("event-hopper-dir")
            case _ => None
        }

    final lazy val tknDirForCoreferencePrediction: Option[String] =
        coreferencePredictConfig match {
            case Some(c) => c.as[Option[String]]("tkn-dir")
            case _ => None
        }

    final lazy val goldTbfForCoreferencePrediction: Option[String] =
        coreferencePredictConfig match {
            case Some(c) => c.as[Option[String]]("gold-tbf")
            case _ => None
        }

    // Training settings of Event Nugget Coreference
    final lazy val coreferenceTrainingCost: Option[Double] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[Double]]("train-cost")
            case _ => None
        }
    final lazy val coreferenceTrainingBias: Option[Double] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[Double]]("train-bias")
            case _ => None
        }
    final lazy val coreferenceModelFile: Option[String] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[String]]("model-file")
            case _ => None
        }
    final lazy val coreferenceLiblinearFeatureFile: Option[String] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[String]]("feature-file")
            case _ => None
        }
    final lazy val coreferenceClassLabelIndexFile: Option[String] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[String]]("class-label-index-file")
            case _ => None
        }
    final lazy val coreferenceFeatureNameIndexFile: Option[String] =
        coreferenceConfig match {
            case Some(c) => c.as[Option[String]]("feature-name-index-file")
            case _ => None
        }

    // Evaluation config
    final private[this] lazy val evaluateConfig: Option[Config] = config.as[Option[Config]]("evaluate")

    final lazy val evaluateScript: Option[String] =
        evaluateConfig match {
            case Some(c) => c.as[Option[String]]("script")
            case _ => None
        }
    final lazy val visualizerScript: Option[String] =
        evaluateConfig match {
            case Some(c) => c.as[Option[String]]("visualizer")
            case _ => None
        }
    final lazy val visualizationTemplate: Option[String] =
        evaluateConfig match {
            case Some(c) => c.as[Option[String]]("visualization-template")
            case _ => None
        }

    // output
    final lazy val outputDir: Option[String] = config.as[Option[String]]("output-dir")

    // cache config
    final lazy val cacheUse: Boolean = config.as[Boolean]("cache.use")
    final lazy val cacheSave: Boolean = config.as[Boolean]("cache.save")
    final lazy val cacheDir: Option[String] = config.as[Option[String]]("cache.dir")
}
