package util

import java.io.File

import config.EventNuggetConfig
import doc.Doc

/**
  * Created by nakayama.
  */
object Cache {
    final val cacheEnable: Boolean = EventNuggetConfig.cacheUse

    final private lazy val docCacheFile: Option[File] =
        EventNuggetConfig.cacheDir match {
            case Some(d) => Some(new File(s"${d}/docs"))
            case _ => None
        }

    private var docCache: Option[Map[String, Doc]] = None

    private def setDocCache(docMapOption: Option[Map[String, Doc]]) =
        docCache = docMapOption

    def loadDocsCache(): Unit = docCacheFile match {
        case Some(f) if f.exists =>
            System.err.println(s"loading cache from ${f.getAbsolutePath} ...")
            docCache = Some(Serializer.load[Map[String, Doc]](f))
        case _ =>
    }

    // Initialize cache
    loadDocsCache()

    def getDocCache(docText: String): Option[Doc] = docCache match {
        case Some(c) => c.get(docText)
        case _ => None
    }

    def saveDocsCache(docs: Seq[Doc]): Unit =
        docCacheFile match {
            case Some(f) =>
                System.err.println(s"saving cache to ${f.getAbsolutePath} ...")
                Serializer.save(docCache.getOrElse(Map.empty[String, Doc]) ++ docs.map(d => (d.text, d)), f)
            case _ => throw new RuntimeException("Cache directory setting was not found.")
        }
}