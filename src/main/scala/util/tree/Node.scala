package util.tree

import event.EventNugget

import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
class Node(_eventNugget: EventNugget,
           parent: Node
          ) {
    def eventNugget: EventNugget = _eventNugget

    var children: ArrayBuffer[Node] = new ArrayBuffer()

    var isClusterTop: Boolean = false

    lazy val subNodes: Array[Node] = Node.dig(this).toArray

    def rootNode: Tree = parent match {
        case r: Tree => r
        case n: Node => n.rootNode
    }

    def add(e: EventNugget): Node = {
        val node: Node = new Node(e, this)
        children += node
        rootNode.nodes += node
        node
    }
}

protected object Node {
    def dig(node: Node): ArrayBuffer[Node] =
        node.children match {
            case ArrayBuffer() => ArrayBuffer(node)
            case children => ArrayBuffer(node) ++ children.flatMap(dig)
        }
}
