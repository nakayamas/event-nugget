package util.tree

import event.EventNugget

import scala.collection.mutable.ArrayBuffer

/**
 * Created by nakayama.
 */
final class Tree extends Node(null, null) {
    override val rootNode = this

    var nodes: ArrayBuffer[Node] = ArrayBuffer(this)

    override def add(e: EventNugget): Node = {
        val node: Node = super.add(e)
        node.isClusterTop = true
        node
    }
}
