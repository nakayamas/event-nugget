package util.nlp

import scala.util.parsing.combinator.RegexParsers

/**
  * Created by nakayama.
  */
object SExpr extends RegexParsers {
    def apply(str: String): Option[Nonterminal] = SExpr.parseAll(s, str) match {
        case Success(root, _) => Some(root)
        case _ => None
    }

    def s: Parser[Nonterminal] = nonterminal

    def node: Parser[Node] = terminal | nonterminal

    def nonterminal: Parser[Nonterminal] = "(" ~> label ~ rep(node) <~ ")" map {
        case label ~ children => new Nonterminal(label, children)
    }

    def terminal: Parser[Terminal] = "(" ~> label ~ word <~ ")" map {
        case label ~ word => new Terminal(label, word)
    }

    def label: Parser[String] = """[^\(\) ]+""".r

    def word: Parser[String] = """[^\(\) ]+""".r
}

abstract case class Node(label: String) {
    def text: String
}

class Nonterminal(label: String, val children: List[Node]) extends Node(label) {
    def text = children.map(_.text).mkString(" ")
}

class Terminal(label: String, val word: String) extends Node(label) {
    def text = word
}