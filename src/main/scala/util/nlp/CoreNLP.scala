package util.nlp

import java.util
import java.util.Properties

import edu.stanford.nlp.ling.CoreAnnotations._
import edu.stanford.nlp.ling.CoreLabel
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}
import edu.stanford.nlp.semgraph.SemanticGraph
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.BasicDependenciesAnnotation
import edu.stanford.nlp.trees.Tree
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation

import scala.collection.JavaConversions._
import scala.collection.mutable

/**
  * Created by nakayama.
  */
final class CoreNLP(text: String) {
    private[this] val properties: Properties = new Properties()
    properties.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, depparse")
    // Disable normalize normal space (U+0020) into no-break space (U+00A0)
    properties.put("tokenize.options",
        Seq("tokenizeNLs=false",
            "invertible=false",
            "ptb3Escaping=false",
            "americanize=false",
            "normalizeAmpersandEntity=false",
            "normalizeSpace=false",
            "normalizeParentheses=false",
            "normalizeOtherBrackets=false",
            "asciiQuotes=false",
            "latexQuotes=false",
            "unicodeQuotes=false").mkString(","))
    private[this] val coreNLP: StanfordCoreNLP = new StanfordCoreNLP(properties)
    val annotation: Annotation = new Annotation(text)
    coreNLP.annotate(annotation)

    private[this] val labels: util.List[CoreLabel] = annotation.get(classOf[TokensAnnotation])

    case class TknWithCoreNLP(label: CoreLabel) {
        val startOffset: Integer = label.get(classOf[CharacterOffsetBeginAnnotation])
        // CoreNLP の end は tkn のものより短い場合がある
        // < で始まっていない場合に末尾に > があるとその分一文字少なくなる
        val endOffset: Integer = label.get(classOf[CharacterOffsetEndAnnotation])
        val surface: String = label.get(classOf[OriginalTextAnnotation])
        val lemma: String = label.get(classOf[LemmaAnnotation])
        val pos: String = label.get(classOf[PartOfSpeechAnnotation])
        val ne: String = label.get(classOf[NamedEntityTagAnnotation])
    }

    val tknsWithCoreNLP: Map[Integer, TknWithCoreNLP] = labels.map { l =>
        val t = TknWithCoreNLP(l)
        (t.startOffset, t)
    }.toMap

    val parseTrees: Seq[Tree] =
        annotation.get(classOf[SentencesAnnotation]).collect {
            case coreMap if coreMap != null => coreMap.get(classOf[TreeAnnotation])
        }

    val allTokenInParseTree: Seq[Tree] =
        parseTrees.flatMap(_.preOrderNodeList()).filter(_.isLeaf)

    val depParseTrees: Seq[SemanticGraph] =
        annotation.get(classOf[SentencesAnnotation]).collect {
            case coreMap if coreMap != null => coreMap.get(classOf[BasicDependenciesAnnotation])
        }.toSeq
}

object CoreNLP {
    final private[this] val coreNLPCache: mutable.Map[String, CoreNLP] = mutable.Map.empty[String, CoreNLP]

    def apply(text: String): CoreNLP = coreNLPCache.getOrElseUpdate(text, new CoreNLP(text))
}