package util.ml.liblinear

import de.bwaldvogel.liblinear.{Feature, FeatureNode, Linear, Model}

/**
  * Created by nakayama.
  */
final case class Predictor(model: Model, liblinearMapper: LiblinearMapper) {
    def predict(features: Map[String, Double]): Int =
        Linear.predict(model, liblinearPredictFormat(features)).toInt

    def predictProbability(features: Map[String, Double]): Double = {
        val probabilityResults = new Array[Double](2)
        Linear.predictProbability(model, liblinearPredictFormat(features), probabilityResults)
        // Class Number: Positive... probabilityResults(0), Negative... probabilityResults(1)
        probabilityResults(0)
    }

    def liblinearPredictFormat(features: Map[String, Double]): Array[Feature] =
        features.collect {
            case (k, v) if liblinearMapper.featureNameIndex.contains(k) =>
                new FeatureNode(liblinearMapper.featureNameIndex(k), v)
        }.toArray
}
