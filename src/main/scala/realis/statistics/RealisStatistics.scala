package realis.statistics

import java.io.File

import config.EventNuggetConfig
import doc.Doc
import event.{EventHopper, Realis}
import reader.TknFilesReader
import scopt.OptionParser
import util.{Cache, FileOps, Measure}

import scala.collection.mutable

/**
  * Created by nakayama.
  */
object RealisStatistics {

    // Read files
    final lazy private[this] val tknFilesForDetectionTraining: Array[File] = //Array(new File("data/train/data/tkn/AFP_ENG_20090801.0002.tab"))
        FileOps.getFilesFromDir(EventNuggetConfig.tknDirForDetectionTraining match {
            case Some(d) => d
            case _ => throw new RuntimeException("Tkn directory setting of the training data of Event Nugget Detection was not found.")
        })
    final lazy private[this] val eventNuggetFilesForDetectionTraining: Array[File] =
        FileOps.getFilesFromDir(EventNuggetConfig.eventNuggetDirForDetectionTraining match {
            case Some(d) => d
            case _ => throw new RuntimeException("Event Nugget directory setting of the training data of Event Nugget Detection was not found.")
        })
    final lazy private[this] val tknFilesForDetectionPrediction: Array[File] = //Array(new File("data/predict/data/tkn/NYT_ENG_20130523.0177.tab"))
        FileOps.getFilesFromDir(EventNuggetConfig.tknDirForDetectionPrediction match {
            case Some(d) => d
            case _ => throw new RuntimeException("Tkn directory setting of the training data of Event Nugget Detection was not found.")
        })
    final lazy private[this] val eventNuggetFilesForDetectionPrediction: Array[File] =
        FileOps.getFilesFromDir(EventNuggetConfig.eventNuggetDirForDetectionPrediction match {
            case Some(d) => d
            case _ => throw new RuntimeException("Event Nugget directory setting of the training data of Event Nugget Detection was not found.")
        })
    final lazy private[this] val tknFilesForCoreferenceTraining: Array[File] = //Array(new File("data/train/data/tkn/AFP_ENG_20090801.0002.tab"))
        FileOps.getFilesFromDir(EventNuggetConfig.tknDirForCoreferenceTraining match {
            case Some(d) => d
            case _ => throw new RuntimeException("Tkn directory setting of the training data of Event Nugget Coreference was not found.")
        })
    final lazy private[this] val eventHopperFilesForCoreferenceTraining: Array[File] =
        FileOps.getFilesFromDir(EventNuggetConfig.eventHopperDirForCoreferenceTraining match {
            case Some(d) => d
            case _ => throw new RuntimeException("Event Hopper directory setting of the training data of Event Nugget Coreference was not found.")
        })
    final lazy private[this] val tknFilesForCoreferencePrediction: Array[File] = //Array(new File("data/predict/data/tkn/NYT_ENG_20130523.0177.tab"))
        FileOps.getFilesFromDir(EventNuggetConfig.tknDirForCoreferencePrediction match {
            case Some(d) => d
            case _ => throw new RuntimeException("Tkn directory setting of the test data of Event Nugget Coreference was not found.")
        })
    final lazy private[this] val eventHopperFilesForCoreferencePrediction: Array[File] =
        FileOps.getFilesFromDir(EventNuggetConfig.eventHopperDirForCoreferencePrediction match {
            case Some(d) => d
            case _ => throw new RuntimeException("Event Hopper directory setting of the test data of Event Nugget Coreference was not found.")
        })

    // Parse files
    final lazy private[this] val eventNuggetDocsForDetectionTraining: Array[Doc] = {
        val docs = new TknFilesReader(tknFilesForDetectionTraining).parseWithEventNuggetFiles(eventNuggetFilesForDetectionTraining)
        if (EventNuggetConfig.cacheSave) Cache.saveDocsCache(docs)
        docs
    }
    final lazy private[this] val eventNuggetDocsForDetectionPrediction: Array[Doc] = {
        val docs = new TknFilesReader(tknFilesForDetectionPrediction).parseWithEventNuggetFiles(eventNuggetFilesForDetectionPrediction)
        if (EventNuggetConfig.cacheSave) Cache.saveDocsCache(docs)
        docs
    }
    final lazy private[this] val eventHopperDocsForCoreferenceTraining: Array[Doc] = {
        val docs = new TknFilesReader(tknFilesForCoreferenceTraining).parseWithEventHopperFiles(eventHopperFilesForCoreferenceTraining)
        if (EventNuggetConfig.cacheSave) Cache.saveDocsCache(docs)
        docs
    }
    final lazy private[this] val eventHopperDocsForCoreferencePrediction: Array[Doc] = {
        val docs = new TknFilesReader(tknFilesForCoreferencePrediction).parseWithEventHopperFiles(eventHopperFilesForCoreferencePrediction)
        if (EventNuggetConfig.cacheSave) Cache.saveDocsCache(docs)
        docs
    }

    // Train and predict by genres
    final lazy private[this] val eventNuggetDetectionTrainingDocsByDomain: Map[Symbol, Array[Doc]] =
        eventNuggetDocsForDetectionTraining.groupBy(_.sourceType.getOrElse('unknown))
    final lazy private[this] val eventNuggetDetectionPredictionDocsByDomain: Map[Symbol, Array[Doc]] =
        eventNuggetDocsForDetectionPrediction.groupBy(_.sourceType.getOrElse('unknown))

    def main(args: Array[String]) {
        // Parse args
        val options: ArgOption = argParser(args)
        if (options.config != null) EventNuggetConfig.set(options.config)

        if (options.countRealis) countRealis()
        if (options.countPos) countPos()
        if (options.incRealis) rateOfInconsistentRealisIncludedHopper()
    }

    final private[this] def rateOfInconsistentRealisIncludedHopper(): Unit = {
        val eventHoppers: Array[EventHopper] = eventHopperDocsForCoreferenceTraining.flatMap(_.eventHoppers.getOrElse(Nil))
        val inconsistentRealisSize: Int = eventHoppers.count { eh =>
            val realisVariation = eh.eventNuggets.map(_.realis).toSet.size
            assume(realisVariation != 0,
                new RuntimeException(s"EventHopper has no realis: [${eh.eventDoc.id}] ${eh}"))
            println(eh.eventDoc.id, eh.id)
            realisVariation != 1
        }
        val rate: String = try {
            Measure.rate(inconsistentRealisSize, eventHoppers.length).toString()
        } catch {
            case e: ArithmeticException => "Nan"
        }

        println(rate)
    }

    final private[this] def countRealis(): Unit = {
        def rate(n: BigDecimal): BigDecimal = n * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)

        // Statistics
        val numberOfRealis: mutable.Map[Realis.Value, BigDecimal] = mutable.Map.empty[Realis.Value, BigDecimal]
        eventNuggetDetectionPredictionDocsByDomain.foreach { case (domain, docs) =>
            println(s"---- ${domain.name} ----")
            val realises: Array[Realis.Value] = docs.flatMap(_.eventNuggets.map(_.realis))
            val numberOfActual = BigDecimal(realises.count(_ == Realis.ACTUAL))
            val numberOfGeneric = BigDecimal(realises.count(_ == Realis.GENERIC))
            val numberOfOther = BigDecimal(realises.count(_ == Realis.OTHER))
            val sumOfRealis = numberOfActual + numberOfGeneric + numberOfGeneric
            numberOfRealis(Realis.ACTUAL) = numberOfRealis.getOrElseUpdate(Realis.ACTUAL, 0) + numberOfActual
            numberOfRealis(Realis.GENERIC) = numberOfRealis.getOrElseUpdate(Realis.GENERIC, 0) + numberOfGeneric
            numberOfRealis(Realis.OTHER) = numberOfRealis.getOrElseUpdate(Realis.OTHER, 0) + numberOfOther
            println(s"actual : ${numberOfActual} ${rate(numberOfActual / sumOfRealis)}")
            println(s"generic: ${numberOfGeneric} ${rate(numberOfGeneric / sumOfRealis)}")
            println(s"other  : ${numberOfOther} ${rate(numberOfOther / sumOfRealis)}")
            println("sum    : " + realises.length)
            println("\n")
        }
        println("---- all domain ----")
        val numberOfActual = numberOfRealis.getOrElse(Realis.ACTUAL, BigDecimal(0))
        val numberOfGeneric = numberOfRealis.getOrElse(Realis.GENERIC, BigDecimal(0))
        val numberOfOther = numberOfRealis.getOrElse(Realis.OTHER, BigDecimal(0))
        val sumOfRealis = numberOfActual + numberOfGeneric + numberOfGeneric
        println(s"actual : ${numberOfActual} ${rate(numberOfActual / sumOfRealis)}")
        println(s"generic: ${numberOfGeneric} ${rate(numberOfGeneric / sumOfRealis)}")
        println(s"other  : ${numberOfOther} ${rate(numberOfOther / sumOfRealis)}")
        println(s"sum    : ${sumOfRealis}")
    }

    // POS
    final private[this] def countPos(): Unit = {
        val posCountByRealisInEachGenre = mutable.Map.empty[Symbol, mutable.Map[Realis.Value, mutable.Map[String, Int]]]
        val posCountByRealis = mutable.Map.empty[Realis.Value, mutable.Map[String, Int]]
        eventNuggetDocsForDetectionTraining.foreach { doc =>
            doc.eventNuggets.foreach { en =>
                val realis = en.realis
                val posCount = posCountByRealis.getOrElse(realis, mutable.Map.empty[String, Int])
                val pos = en.headTkn match {
                    case Some(t) => t.pos
                    case _ => throw new RuntimeException( s"""A head tkn of "${en.text}" in [${doc.name}] was not found.""")
                }
                posCount(pos) = posCount.getOrElseUpdate(pos, 0) + 1
                posCountByRealis(realis) = posCount
            }
            doc.sourceType match {
                case Some(st) => posCountByRealisInEachGenre(st) = posCountByRealis
                case _ => new RuntimeException(doc.name + " doesn't have source type.")
            }
        }

        // Number of events in each POS
        posCountByRealisInEachGenre.foreach { case (genre, posCountByRealis) =>
            println(s"---- ${genre} ----")
            posCountByRealis.foreach { case (realis, pos) =>
                println(s"[${realis}]")
                pos.foreach { case (p, c) =>
                    println(s"${p}: ${c}")
                }
            }
        }
    }

    final private case class ArgOption(config: File = null,
                                       incRealis: Boolean = false,
                                       countRealis: Boolean = false,
                                       countPos: Boolean = false)

    final private[this] def argParser(args: Array[String]): ArgOption =
        new OptionParser[ArgOption](this.getClass.getName) {
            opt[File]('c', "config")
                .action((x, c) => c.copy(config = x))
                .validate { x =>
                    if (!x.exists) {
                        failure("configuration file was not found: " + x.getAbsolutePath)
                    } else success
                }
                .text("configuration file")

            opt[Unit]("inc-realis")
                .action((_, c) => c.copy(incRealis = true))
                .text("enable inc-realis mode")
            opt[Unit]("count-realis")
                .action((_, c) => c.copy(countRealis = true))
                .text("enable count-realis mode")
            opt[Unit]("count-realis")
                .action((_, c) => c.copy(countPos = true))
                .text("enable count-pos mode")
        }.parse(args, ArgOption()) match {
            case Some(x) => x
            case _ => System.exit(-1); null
        }

}
