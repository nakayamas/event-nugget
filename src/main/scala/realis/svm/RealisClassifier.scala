package realis.svm

import java.io.File

import config.EventNuggetConfig
import de.bwaldvogel.liblinear._
import doc.Doc
import event._
import org.apache.commons.cli.{CommandLine, DefaultParser, HelpFormatter, Options}
import reader.TknFilesReader
import realis.RealisFeatureExtractor
import util.ml.liblinear.{LiblinearFormatter, LiblinearMapper, Predictor}
import util.{Cache, FileOps}

import scala.collection.mutable

/**
  * Created by nakayama.
  */
object RealisClassifier {
    def main(args: Array[String]): Unit = {
        val cl: CommandLine = parseArg(args)

        // Read configurations
        if (cl.hasOption("c")) EventNuggetConfig.set(new File(cl.getOptionValue("config")))

        // Parse args
        val trainTknFiles: Array[File] = //Array(new File("data/train/data/tkn/AFP_ENG_20090801.0002.tab"))
            FileOps.getFilesFromDir(EventNuggetConfig.tknDirForDetectionTraining match {
                case Some(d) => d
                case _ => throw new RuntimeException("Tkn directory  setting of the training data of Event Nugget Detection was not found.")
            })
        val trainEventNuggetFiles: Array[File] =
            FileOps.getFilesFromDir(EventNuggetConfig.eventNuggetDirForDetectionTraining match {
                case Some(d) => d
                case _ => throw new RuntimeException("Event Nugget directory setting of the training data of Event Nugget Detection was not found.")
            })
        val predictTknFiles: Array[File] = //Array(new File("data/predict/data/tkn/NYT_ENG_20130523.0177.tab"))
            FileOps.getFilesFromDir(EventNuggetConfig.tknDirForDetectionPrediction match {
                case Some(d) => d
                case _ => throw new RuntimeException("Tkn directory setting of the training data of Event Nugget Detection was not found.")
            })
        val predictEventNuggetFiles: Array[File] =
            FileOps.getFilesFromDir(EventNuggetConfig.eventNuggetDirForDetectionPrediction match {
                case Some(d) => d
                case _ => throw new RuntimeException("Event Nugget directory setting of the training data of Event Nugget Detection was not found.")
            })
        val cost: Double = {
            if (cl.hasOption("cost")) {
                cl.getOptionValue("cost").toDouble
            } else {
                EventNuggetConfig.realisTrainCost match {
                    case Some(c) => c
                    case _ => throw new RuntimeException("Training cost setting of Realis was not found.")
                }
            }
        }
        val bias: Double = {
            if (cl.hasOption("bias")) {
                cl.getOptionValue("bias").toDouble
            } else {
                EventNuggetConfig.realisTrainBias match {
                    case Some(b) => b
                    case _ => throw new RuntimeException("Training bias setting of Realis was not found.")
                }
            }
        }
        val outputDir: File = FileOps.withMakeParentDirs(
            if (cl.hasOption("o")) cl.getOptionValue("output-dir")
            else EventNuggetConfig.outputDir match {
                case Some(d) if d != "" => d
                case _ => throw new RuntimeException("Output directory setting was not found.")
            })

        // Parse files
        val allTrainDoc: Array[Doc] =
            new TknFilesReader(trainTknFiles).parseWithEventNuggetFiles(trainEventNuggetFiles)
        val allPredictDoc: Array[Doc] =
            new TknFilesReader(predictTknFiles).parseWithEventNuggetFiles(predictEventNuggetFiles)
        if (EventNuggetConfig.cacheSave)
            Cache.saveDocsCache(allTrainDoc ++ allPredictDoc)

        // Train and predict by genres
        val trainDocsByDomain: Map[Symbol, Array[Doc]] = allTrainDoc.groupBy(_.sourceType.getOrElse('unknown))
        val predictDocsByDomain: Map[Symbol, Array[Doc]] = allPredictDoc.groupBy(_.sourceType.getOrElse('unknown))

        // Train
        trainDocsByDomain.foreach { case (domain, docs) =>
            train(docs, cost, bias, FileOps.withMakeParentDirs(s"${outputDir.getAbsolutePath}/${domain.name}"))
        }

        // Predict
        val predictedDocsByDomain: Map[Symbol, Seq[PredictedEventDoc]] =
            predictDocsByDomain.map { case (domain, docs) =>
                domain -> predict(docs, FileOps.withMakeParentDirs(s"${outputDir.getAbsolutePath}/${domain.name}"))
            }

        // Calculate accuracy
        val result = new StringBuilder
        var tpSum = 0
        var fnSum = 0
        val tpSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        val fnSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        val fpSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        val tnSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        predictedDocsByDomain.foreach { case (domain, predictedDocs) =>
            result.append(s"---- ${domain.name} ----\n")

            // True Positive
            val tp: Int = predictedDocs.foldLeft(0) { (z, pd) =>
                z + pd.predictedEventNuggets.count(pen => pen.predictedRealis.orNull == pen.realis)
            }
            tpSum += tp
            // False Negative
            val fn: Int = predictedDocs.flatMap(_.predictedEventNuggets).length - tp
            fnSum += fn

            // Precision ond Recall for each realis
            Realis.values.foreach { r =>
                result.append(s"[REALIS]: ${r.toString}").append("\n")

                val tpByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count(pen =>
                        pen.predictedRealis.orNull == r && pen.realis == r)
                }
                tpSumByRealis(r) = tpSumByRealis.getOrElse(r, 0) + tpByRealis

                val fnByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count(pen =>
                        pen.realis == r && pen.predictedRealis.orNull != r)
                }
                fnSumByRealis(r) = fnSumByRealis.getOrElse(r, 0) + fnByRealis

                val tnByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count(pen =>
                        pen.realis != r && pen.predictedRealis.orNull != r)
                }
                tnSumByRealis(r) = tnSumByRealis.getOrElse(r, 0) + tnByRealis

                val fpByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count(pen =>
                        pen.predictedRealis.orNull == r && pen.realis != r)
                }
                fpSumByRealis(r) = fpSumByRealis.getOrElse(r, 0) + fpByRealis

                result.append(s"true  positive: ${tpByRealis}").append("\n")
                result.append(s"false negative: ${fnByRealis}").append("\n")
                result.append(s"false positive: ${fpByRealis}").append("\n")
                result.append(s"true  negative: ${tnByRealis}").append("\n")

                val precisionByRealis = BigDecimal(tpByRealis) / (BigDecimal(tpByRealis + fpByRealis) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"Precision: ").append {
                    precisionByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
                val recallByRealis = BigDecimal(tpByRealis) / (BigDecimal(tpByRealis + fnByRealis) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"Recall   : ").append {
                    recallByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
                val fByRealis = 2 * recallByRealis * precisionByRealis / ((recallByRealis + precisionByRealis) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"F-measure: ").append {
                    fByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
            }
            result.append("\n")

            result.append("[All REALIS]").append("\n")
            result.append(s"true  positive: ${tp}").append("\n")
            result.append(s"false negative: ${fn}").append("\n")

            result.append("Accuracy: ").append {
                (BigDecimal(tp) / BigDecimal(tp + fn)) * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n\n")
        }
        result.append("---- all domain ----\n")

        // Precision ond Recall for each realis
        Realis.values.foreach { r =>
            result.append(s"[REALIS]: ${r.toString}").append("\n")

            result.append(s"true  positive: ${tpSumByRealis(r)}").append("\n")
            result.append(s"false negative: ${fnSumByRealis(r)}").append("\n")
            result.append(s"false positive: ${fpSumByRealis(r)}").append("\n")
            result.append(s"true  negative: ${tnSumByRealis(r)}").append("\n")

            val precisionByRealis = BigDecimal(tpSumByRealis(r)) / (BigDecimal(tpSumByRealis(r) + fpSumByRealis(r)) match {
                case n if n * 2 == 0 => 1
                case n => n
            })
            result.append(s"Precision: ").append {
                precisionByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n")
            val recallByRealis = BigDecimal(tpSumByRealis(r)) / (BigDecimal(tpSumByRealis(r) + fnSumByRealis(r)) match {
                case n if n * 2 == 0 => 1
                case n => n
            })
            result.append(s"Recall   : ").append {
                recallByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n")
            val fByRealis = 2 * recallByRealis * precisionByRealis / ((recallByRealis + precisionByRealis) match {
                case n if n * 2 == 0 => 1
                case n => n
            })
            result.append(s"F-measure: ").append {
                fByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n")
        }
        result.append("\n")

        result.append(s"true  positive: ${tpSum}").append("\n")
        result.append(s"false negative: ${fnSum}").append("\n")
        result.append("Accuracy: ").append {
            BigDecimal(tpSum) / (BigDecimal(tpSum + fnSum) match {
                case n if n * 2 == 0 => 1
                case n => n
            }) * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
        }

        // Output
        println(result)
        val resultFile: File = new File(s"${outputDir.getAbsolutePath}/result")
        FileOps.writeWithMakeParentDirs(resultFile)(_.println(result))
    }

    def train(docs: Array[Doc], cost: Double, bias: Double, outputDir: File): Unit = {
        // Prepare model and feature files
        val modelFile: File = new File(EventNuggetConfig.realisModelFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Model file setting of Realis was not found.")
        })
        val liblinearFeatureFile: File = new File(EventNuggetConfig.realisLiblinearFeatureFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Liblinear feature file setting of Realis was not found.")
        })
        val classLabelIndexFile: File = new File(EventNuggetConfig.realisClassLabelIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Class label index file setting of Realis was not found.")
        })
        val featureNameIndexFile: File = new File(EventNuggetConfig.realisFeatureNameIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Feature name index file setting of Realis was not found.")
        })

        // Extract feature
        val featureExtractor: RealisFeatureExtractor = new RealisFeatureExtractor('training)
        featureExtractor.createFeatures(for {
            doc <- docs
            eventNuggetDoc <- doc.eventNuggetDoc
        } yield eventNuggetDoc)

        // Map feature name to number (Liblinear format)
        val liblinearMapper: LiblinearMapper = LiblinearMapper(featureExtractor.samples)
        LiblinearFormatter(featureExtractor.samples, liblinearMapper).saveAsLiblinearTrainFormat(liblinearFeatureFile)
        liblinearMapper.saveClassLabelIndex(classLabelIndexFile)
        liblinearMapper.saveFeatureNameIndex(featureNameIndexFile)

        // Train with LibLinear
        val parameter = new Parameter(SolverType.L2R_L2LOSS_SVC_DUAL, cost, Double.PositiveInfinity)
        val problem = Problem.readFromFile(liblinearFeatureFile, bias)
        val model = Linear.train(problem, parameter)
        model.save(modelFile)
    }

    def predict(docs: Array[Doc], modelAndFeatureDir: File): Seq[PredictedEventDoc] = {
        docs.map { d =>
            // Read model and feature files
            val modelFile: File = new File(EventNuggetConfig.realisModelFile match {
                case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
                case _ => throw new RuntimeException("Model file setting of Realis was not found.")
            })
            val classLabelIndexFile: File = new File(EventNuggetConfig.realisClassLabelIndexFile match {
                case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
                case _ => throw new RuntimeException("Class label index file setting of Realis was not found.")
            })
            val featureNameIndexFile: File = new File(EventNuggetConfig.realisFeatureNameIndexFile match {
                case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
                case _ => throw new RuntimeException("Feature name index file setting of Realis was not found.")
            })

            // Load model and features
            val model: Model = Model.load(modelFile)
            val liblinearMapper = LiblinearMapper(classLabelIndexFile, featureNameIndexFile)
            val predictor = new Predictor(model, liblinearMapper)
            val extractor = new RealisFeatureExtractor('predict)
            new PredictedEventDoc(d, predictedEventNuggets = d.eventNuggets.map { en =>
                val predictedClassIndex: Int = predictor.predict(extractor.extractFeatures(en).toMap)
                val predictedRealis: Realis.Value = liblinearMapper.classIndexToClassLabel(predictedClassIndex) match {
                    case Some(n) => Realis.withName(n.toUpperCase)
                    case _ => throw new RuntimeException("Unknown class index: " + predictedClassIndex)
                }
                new PredictedEventNugget(en, predictedRealis = Some(predictedRealis))
            })
        }
    }

    final private[this] def parseArg(args: Array[String]): CommandLine = {
        val parser = new DefaultParser()
        val options = new Options()
        options.addOption("c", "config", true, "Path to the config file.")
        options.addOption("cost", true, "Cost parameter for Liblinear training.")
        options.addOption("bias", true, "Bias parameter for Liblinear training.")
        options.addOption("o", "output-dir", true, "Path to the output dir (not specific file).")
        options.addOption("cache", false, "Use parsing cache or not. (default false)")

        val cl = parser.parse(options, args)
        if (cl.hasOption("h")) {
            new HelpFormatter().printHelp("SentenceReconstructor", options, true)
            System.exit(0)
        }

        cl
    }
}