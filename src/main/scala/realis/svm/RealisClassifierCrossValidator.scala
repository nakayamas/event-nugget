package realis.svm

import java.io.File

import config.EventNuggetConfig
import de.bwaldvogel.liblinear._
import doc.Doc
import event._
import org.apache.commons.cli.{CommandLine, DefaultParser, HelpFormatter, Options}
import reader.TknFilesReader
import realis.RealisFeatureExtractor
import util.ml.liblinear.{LiblinearFormatter, LiblinearMapper, Predictor}
import util.{Cache, FileOps}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
object RealisClassifierCrossValidator {
    def main(args: Array[String]): Unit = {
        val cl: CommandLine = parseArg(args)

        // Read configurations
        if (cl.hasOption("c")) EventNuggetConfig.set(new File(cl.getOptionValue("config")))

        // Parse args
        val numberOfFolds: Int = if (cl.hasOption("n")) cl.getOptionValue("n-fold").toInt else 5
        val nFoldedTknFiles: Map[Symbol, Array[File]] =
            FileOps.getFilesFromDir(EventNuggetConfig.tknDirForDetectionTraining match {
                case Some(d) => d
                case _ => throw new RuntimeException("Tkn directory setting of the training data of Event Nugget Detection was not found.")
            }).zipWithIndex.groupBy {
                case (tfs, i) => Symbol(s"n${i % numberOfFolds}")
            }.map {
                case (nFold, tknFilesWithIndex) => nFold -> tknFilesWithIndex.map(_._1)
            }
        val trainEventNuggetFiles: Array[File] =
            FileOps.getFilesFromDir(EventNuggetConfig.eventNuggetDirForDetectionTraining match {
                case Some(d) => d
                case _ => throw new RuntimeException("Event Nugget directory setting of the training data of Event Nugget Detection was not found.")
            })
        val costs: Seq[Double] = Seq(0.01) // Seq(0.001, 0.005, 0.01, 0.05, 0.1, 0.5, 1.0, 10.0)
        val biases: Seq[Double] = Seq(-1) // Seq(-1, 0, 1)
        val outputDir: File = FileOps.withMakeParentDirs(
            if (cl.hasOption("o")) cl.getOptionValue("output-dir")
            else EventNuggetConfig.outputDir match {
                case Some(d) if d != "" => d
                case _ => throw new RuntimeException("Output directory setting was not found.")
            })

        // Cross validate
        val accuracies: mutable.Map[String, ArrayBuffer[BigDecimal]] =
            mutable.Map.empty[String, ArrayBuffer[BigDecimal]]
        for {
            cost <- costs
            bias <- biases
            (nFold, predictTknFiles) <- nFoldedTknFiles
            trainTknFiles: Array[File] = nFoldedTknFiles.collect { case (n, tfs) if n != nFold => tfs }.flatten.toArray
        } {
            // Output directory
            val nFoldOutputDirPath: String = s"${outputDir.getAbsolutePath}/c${cost}_b${bias}_${nFold.name}"

            val usedFiles = new StringBuilder
            usedFiles.append("Doc files of training data are below: ").append("\n")
            trainTknFiles.foreach(f => usedFiles.append(f.getName).append("\n"))
            usedFiles.append("\n")
            usedFiles.append("Doc files of prediction data are below: ").append("\n")
            predictTknFiles.foreach(f => usedFiles.append(f.getName).append("\n"))

            println(usedFiles.result())
            FileOps.writeWithMakeParentDirs(new File(s"${nFoldOutputDirPath}/used_files.txt")) {
                _.println(usedFiles)
            }

            // Parse files
            val allTrainDoc: Array[Doc] =
                new TknFilesReader(trainTknFiles).parseWithEventNuggetFiles(trainEventNuggetFiles)
            val allPredictDoc: Array[Doc] =
                new TknFilesReader(predictTknFiles).parseWithEventNuggetFiles(trainEventNuggetFiles)
            if (EventNuggetConfig.cacheSave)
                Cache.saveDocsCache(allTrainDoc ++ allPredictDoc)

            // Train and predict by genres
            val trainDocsByDomain: Map[Symbol, Array[Doc]] = allTrainDoc.groupBy(_.sourceType.getOrElse('unknown))
            val predictDocsByDomain: Map[Symbol, Array[Doc]] = allPredictDoc.groupBy(_.sourceType.getOrElse('unknown))

            // Train
            trainDocsByDomain.foreach { case (domain, docs) =>
                val trainingResultOutputDir =
                    FileOps.withMakeParentDirs(s"${nFoldOutputDirPath}/${domain.name}")
                train(docs, cost, bias, trainingResultOutputDir)
            }

            // Predict
            val predictedDocsByDomain: Map[Symbol, Seq[PredictedEventDoc]] =
                predictDocsByDomain.map { case (domain, docs) =>
                    val trainingResultOutputDir =
                        FileOps.withMakeParentDirs(s"${nFoldOutputDirPath}/${domain.name}")
                    domain -> predict(docs, trainingResultOutputDir)
                }

            // Calculate accuracy
            val result = new StringBuilder
            var tpSum = 0
            var fnSum = 0
            val tpSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
            val fnSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
            val fpSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
            val tnSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
            predictedDocsByDomain.foreach { case (domain, predictedDocs) =>
                result.append(s"---- ${domain.name} ----\n")

                // True Positive
                val tp: Int = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count(pen => pen.predictedRealis.orNull == pen.realis)
                }
                tpSum += tp
                // False Negative
                val fn: Int = predictedDocs.flatMap(_.predictedEventNuggets).length - tp
                fnSum += fn

                // Precision ond Recall for each realis
                Realis.values.foreach { r =>
                    // Output instances
                    // doc-id event-id start-offset event-surface url
                    val outputInstances = new StringBuilder
                    def eventOutputTemplate(predictedEventNugget: PredictedEventNugget) = {
                        val docId = predictedEventNugget.eventDoc.id
                        val eventId = predictedEventNugget.id.replace("em-", "E")
                        val url = s"""http://cairo.lti.cs.cmu.edu:10080/index.xhtml#/ann/${docId}?focus=${eventId}"""
                        s"${docId}\t${eventId}\t${predictedEventNugget.startOffset}\t${predictedEventNugget.predictedRealis.get.toString}\t${predictedEventNugget.text}\t${url}"
                    }

                    // Calculate
                    result.append(s"[REALIS]: ${r.toString}").append("\n")
                    outputInstances.append(s"[REALIS]: ${r.toString}").append("\n")
                    outputInstances.append(s"doc-id\tevent-id\tstart-offset\tpredicted-realis\tevent-surface\turl").append("\n")

                    outputInstances.append("[true positive]\n")
                    val tpByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                        z + pd.predictedEventNuggets.count { pen =>
                            val isTp = pen.predictedRealis.orNull == r && pen.realis == r
                            if (isTp) {
                                outputInstances.append(eventOutputTemplate(pen)).append("\n")
                            }
                            isTp
                        }
                    }
                    tpSumByRealis(r) = tpSumByRealis.getOrElse(r, 0) + tpByRealis
                    outputInstances.append("\n")

                    outputInstances.append("[false negative]\n")
                    val fnByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                        z + pd.predictedEventNuggets.count { pen =>
                            val isFn = pen.realis == r && pen.predictedRealis.orNull != r
                            if (isFn) {
                                outputInstances.append(eventOutputTemplate(pen)).append("\n")
                            }
                            isFn
                        }
                    }
                    fnSumByRealis(r) = fnSumByRealis.getOrElse(r, 0) + fnByRealis
                    outputInstances.append("\n")

                    outputInstances.append("[true negative]\n")
                    val tnByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                        z + pd.predictedEventNuggets.count { pen =>
                            val isTn = pen.realis != r && pen.predictedRealis.orNull != r
                            if (isTn) {
                                outputInstances.append(eventOutputTemplate(pen)).append("\n")
                            }
                            isTn
                        }
                    }
                    tnSumByRealis(r) = tnSumByRealis.getOrElse(r, 0) + tnByRealis
                    outputInstances.append("\n")

                    outputInstances.append("[false positive]\n")
                    val fpByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                        z + pd.predictedEventNuggets.count { pen =>
                            val isFp = pen.predictedRealis.orNull == r && pen.realis != r
                            if (isFp) {
                                outputInstances.append(eventOutputTemplate(pen)).append("\n")
                            }
                            isFp
                        }
                    }
                    fpSumByRealis(r) = fpSumByRealis.getOrElse(r, 0) + fpByRealis
                    outputInstances.append("\n")

                    // Output instances
                    FileOps.writeWithMakeParentDirs(new File(s"${nFoldOutputDirPath}/${domain.name}/${r.toString}_instances.tsv")) {
                        _.println(outputInstances.result())
                    }

                    result.append(s"true  positive: ${tpByRealis}").append("\n")
                    result.append(s"false negative: ${fnByRealis}").append("\n")
                    result.append(s"false positive: ${fpByRealis}").append("\n")
                    result.append(s"true  negative: ${tnByRealis}").append("\n")

                    val precisionByRealis = BigDecimal(tpByRealis) / (BigDecimal(tpByRealis + fpByRealis) match {
                        case n if n * 2 == 0 => 1
                        case n => n
                    })
                    result.append(s"Precision: ").append {
                        precisionByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                    }.append("\n")
                    val recallByRealis = BigDecimal(tpByRealis) / (BigDecimal(tpByRealis + fnByRealis) match {
                        case n if n * 2 == 0 => 1
                        case n => n
                    })
                    result.append(s"Recall   : ").append {
                        recallByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                    }.append("\n")
                    val fByRealis = 2 * recallByRealis * precisionByRealis / ((recallByRealis + precisionByRealis) match {
                        case n if n * 2 == 0 => 1
                        case n => n
                    })
                    result.append(s"F-measure: ").append {
                        fByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                    }.append("\n")
                }
                result.append("\n")

                result.append("[All REALIS]").append("\n")
                result.append(s"true  positive: ${tp}").append("\n")
                result.append(s"false negative: ${fn}").append("\n")

                result.append("Accuracy: ").append {
                    (BigDecimal(tp) / BigDecimal(tp + fn)) * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n\n")
            }
            result.append("---- all domain ----\n")

            // Precision ond Recall for each realis
            Realis.values.foreach { r =>
                result.append(s"[REALIS]: ${r.toString}").append("\n")

                result.append(s"true  positive: ${tpSumByRealis(r)}").append("\n")
                result.append(s"false negative: ${fnSumByRealis(r)}").append("\n")
                result.append(s"false positive: ${fpSumByRealis(r)}").append("\n")
                result.append(s"true  negative: ${tnSumByRealis(r)}").append("\n")

                val precisionByRealis = BigDecimal(tpSumByRealis(r)) / (BigDecimal(tpSumByRealis(r) + fpSumByRealis(r)) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"Precision: ").append {
                    precisionByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
                val recallByRealis = BigDecimal(tpSumByRealis(r)) / (BigDecimal(tpSumByRealis(r) + fnSumByRealis(r)) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"Recall   : ").append {
                    recallByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
                val fByRealis = 2 * recallByRealis * precisionByRealis / ((recallByRealis + precisionByRealis) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"F-measure: ").append {
                    fByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
            }
            result.append("\n")

            result.append(s"true  positive: ${tpSum}").append("\n")
            result.append(s"false negative: ${fnSum}").append("\n")
            val accuracy =
                BigDecimal(tpSum) / (BigDecimal(tpSum + fnSum) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                }) * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            result.append("Accuracy: ").append(accuracy).append("\n")
            accuracies.getOrElseUpdate(s"c${cost}_b${bias}", ArrayBuffer[BigDecimal]()) += accuracy

            // Output
            println(result)
            FileOps.writeWithMakeParentDirs(new File(s"${nFoldOutputDirPath}/result")) {
                _.println(result)
            }
        }

        // Output a best parameter
        val bestParameter: (String, BigDecimal) = accuracies.map { case (p, n) =>
            p -> n.sum / numberOfFolds
        }.maxBy { case (_, n) => n }
        println(bestParameter)
        FileOps.writeWithMakeParentDirs(new File(s"${outputDir.getAbsolutePath}/result")) {
            _.println(bestParameter)
        }
    }

    def train(docs: Array[Doc], cost: Double, bias: Double, outputDir: File): Unit = {
        // Prepare model and feature files
        val modelFile: File = new File(EventNuggetConfig.realisModelFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Model file setting of Realis was not found.")
        })
        val liblinearFeatureFile: File = new File(EventNuggetConfig.realisLiblinearFeatureFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Liblinear feature file setting of Realis was not found.")
        })
        val classLabelIndexFile: File = new File(EventNuggetConfig.realisClassLabelIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Class label index file setting of Realis was not found.")
        })
        val featureNameIndexFile: File = new File(EventNuggetConfig.realisFeatureNameIndexFile match {
            case Some(f) => s"${outputDir.getAbsolutePath}/${f}"
            case _ => throw new RuntimeException("Feature name index file setting of Realis was not found.")
        })

        // Extract feature
        val featureExtractor: RealisFeatureExtractor = new RealisFeatureExtractor('training)
        featureExtractor.createFeatures(for {
            doc <- docs
            eventNuggetDoc <- doc.eventNuggetDoc
        } yield eventNuggetDoc)

        // Map feature name to number (Liblinear format)
        val liblinearMapper: LiblinearMapper = LiblinearMapper(featureExtractor.samples)
        LiblinearFormatter(featureExtractor.samples, liblinearMapper).saveAsLiblinearTrainFormat(liblinearFeatureFile)
        liblinearMapper.saveClassLabelIndex(classLabelIndexFile)
        liblinearMapper.saveFeatureNameIndex(featureNameIndexFile)

        // Train with LibLinear
        val parameter = new Parameter(SolverType.L2R_L2LOSS_SVC_DUAL, cost, Double.PositiveInfinity)
        val problem = Problem.readFromFile(liblinearFeatureFile, bias)
        val model = Linear.train(problem, parameter)
        model.save(modelFile)
    }

    def predict(docs: Array[Doc], modelAndFeatureDir: File): Seq[PredictedEventDoc] = {
        docs.map { d =>
            // Read model and feature files
            val modelFile: File = new File(EventNuggetConfig.realisModelFile match {
                case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
                case _ => throw new RuntimeException("Model file setting of Realis was not found.")
            })
            val classLabelIndexFile: File = new File(EventNuggetConfig.realisClassLabelIndexFile match {
                case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
                case _ => throw new RuntimeException("Class label index file setting of Realis was not found.")
            })
            val featureNameIndexFile: File = new File(EventNuggetConfig.realisFeatureNameIndexFile match {
                case Some(f) => s"${modelAndFeatureDir.getAbsolutePath}/${f}"
                case _ => throw new RuntimeException("Feature name index file setting of Realis was not found.")
            })

            // Load model and features
            val model: Model = Model.load(modelFile)
            val liblinearMapper = LiblinearMapper(classLabelIndexFile, featureNameIndexFile)
            val predictor = new Predictor(model, liblinearMapper)
            val extractor = new RealisFeatureExtractor('predict)
            new PredictedEventDoc(d, predictedEventNuggets = d.eventNuggets.map { en =>
                val predictedClassIndex: Int = predictor.predict(extractor.extractFeatures(en).toMap)
                val predictedRealis: Realis.Value = liblinearMapper.classIndexToClassLabel(predictedClassIndex) match {
                    case Some(n) => Realis.withName(n.toUpperCase)
                    case _ => throw new RuntimeException("Unknown class index: " + predictedClassIndex)
                }
                new PredictedEventNugget(en, predictedRealis = Some(predictedRealis))
            })
        }
    }

    final private[this] def parseArg(args: Array[String]): CommandLine = {
        val parser = new DefaultParser()
        val options = new Options()
        options.addOption("c", "config", true, "Path to the config file.")
        options.addOption("n", "n-fold", true, "Partitioned into n equal sized subsamples for a cross validation. (default 5)")
        options.addOption("o", "output-dir", true, "Path to the output dir (not specific file).")
        options.addOption("cache", false, "Use parsing cache or not. (default false)")

        val cl = parser.parse(options, args)
        if (cl.hasOption("h")) {
            new HelpFormatter().printHelp("SentenceReconstructor", options, true)
            System.exit(0)
        }

        cl
    }
}
