package realis

import event.{EventNugget, EventNuggetDoc}
import util.ml.liblinear.LiblinearInstance

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

/**
  * Created by nakayama.
  */
final case class RealisFeatureExtractor(mode: Symbol = 'predict) {
    var samples: ArrayBuffer[LiblinearInstance] = ArrayBuffer[LiblinearInstance]()

    def createFeatures(eventNuggetDocs: Array[EventNuggetDoc]): Unit =
        eventNuggetDocs.foreach(createFeatures)

    def createFeatures(eventNuggetDoc: EventNuggetDoc): Unit =
        createFeatures(eventNuggetDoc.eventNuggets)

    def createFeatures(eventNuggets: Seq[EventNugget]): Unit =
        eventNuggets.foreach { en =>
            samples += new LiblinearInstance(en.realis.toString, extractFeatures(en))
        }

    def extractFeatures(eventNugget: EventNugget): mutable.Map[String, Double] = {
        val features: mutable.Map[String, Double] = mutable.Map.empty[String, Double]

        features.put("surface_" + eventNugget.text.replace(" ", "_"), 1)
        features.put("pos2_" + eventNugget.headTkn.get.pos2, 1)
        features.put("main_verb_surface_" + (eventNugget.headTkn.get.mainVerbInSentence match {
            case Some(mv) => mv.nodeString.replace(" ", "_")
            case _ => "none"
        }), 1)
        features.put("pre_tkn_lemma_" + (eventNugget.headTkn.get.prevTkn match {
            case Some(pt) if pt.lemma != null => pt.lemma.replace(" ", "_")
            case _ => ""
        }), 1)
        features.put("past", featureValueConverter(
            getTense(eventNugget) match {
                case t if t == "past" => true
                case _ => false
            }))
        features.put("present", featureValueConverter(
            getTense(eventNugget) match {
                case t if t == "present" => true
                case _ => false
            }))

        features.put("determined", featureValueConverter(determined(eventNugget)))

        features.put("other_candidate_by_rule", featureValueConverter(
            getTense(eventNugget) match {
                case t if (t != "present" &&
                    (isFutureSentence(eventNugget)
                        || isConditionalSentence(eventNugget)
                        || isQuestionSentence(eventNugget)
                        || isNegationSentence(eventNugget)))
                => true
                case _ => false
            }))

        //        features.put("type_" + eventNugget.eventTypeExp, 1)
        //        features.put("subtype_" + eventNugget.eventSubtype, 1)
        //        features.put("type_subtype_" + eventNugget.eventType + "_" + eventNugget.eventSubtype, 1)

        features.put("future_sentence", featureValueConverter(isFutureSentence(eventNugget)))
        features.put("conditional_sentence", featureValueConverter(isConditionalSentence(eventNugget)))
        features.put("question_sentence", featureValueConverter(isQuestionSentence(eventNugget)))
        features.put("including_negation", featureValueConverter(isNegationSentence(eventNugget)))

        features
    }

    def isFutureSentence(eventNugget: EventNugget): Boolean =
        eventNugget.headTkn.get.sentenceNodesInParseTree.exists(_.nodeString() == "will")

    def isConditionalSentence(eventNugget: EventNugget): Boolean =
        eventNugget.headTkn.get.sentenceNodesInParseTree.exists(_.nodeString() == "if")

    def isQuestionSentence(eventNugget: EventNugget): Boolean =
        eventNugget.headTkn.get.sentenceNodesInParseTree.last.nodeString() == "?"

    def isNegationSentence(eventNugget: EventNugget): Boolean =
        eventNugget.headTkn.get.sentenceNodesInParseTree.exists(_.nodeString() == "not")

    def getTense(eventNugget: EventNugget): String = eventNugget.headTkn match {
        case Some(t) if t.pos2 == "VB" =>
            t.prevTkn match {
                case Some(pt) if pt.pos == "TO" => pt.mainVerbInSentence match {
                    case Some(mv) => getTense(mv.nodeString.take(3).mkString.trim)
                    case _ => "unknown"
                }
                case _ => getTense(t.pos)
            }
        case Some(t) if t.pos2 == "NN" => t.mainVerbInSentence match {
            case Some(mv) => getTense(mv.nodeString.take(3).mkString.trim)
            case _ => "unknown"
        }
        case Some(t) => getTense(t.pos)
        case _ => "unknown"
    }

    def getTense(pos: String): String = {
        if (pos == "VBD" || pos == "VBN") "past"
        else if (pos == "VBG" || pos == "VBP" || pos == "VBZ") "present"
        else "unknown"
    }

    def determined(eventNugget: EventNugget): Boolean = {
        eventNugget.headTkn match {
            case Some(t) if t.pos2 == "NN" =>
                t.prevTkn match {
                    case Some(pt) =>
                        if (pt.pos == "DT") true
                        else pt.prevTkn match {
                            case Some(ppt) =>
                                if (pt.pos == "DT") true
                                else false
                            case _ => false
                        }
                    case _ => false
                }
            case _ => false
        }
    }

    def featureValueConverter(v: Any): Double =
        v match {
            case i: Int => i
            case d: Double => d
            case b: Boolean => if (b) 1 else 0
            case null => 0
        }
}
