package realis.rule

import event.{EventNugget, Realis}
import realis.RealisFeatureExtractor

/**
  * Created by nakayama.
  */
class RuleBasedPredictor {
    val predictModeRealisFeatureExtractor: RealisFeatureExtractor = new RealisFeatureExtractor('predict)

    def predict(eventNugget: EventNugget): Realis.Value = {
        predictModeRealisFeatureExtractor.getTense(eventNugget) match {
            case "present" => Realis.GENERIC
            case _ if (predictModeRealisFeatureExtractor.isFutureSentence(eventNugget)
                || predictModeRealisFeatureExtractor.isConditionalSentence(eventNugget)
                || predictModeRealisFeatureExtractor.isQuestionSentence(eventNugget)
                || predictModeRealisFeatureExtractor.isNegationSentence(eventNugget))
            => Realis.OTHER
            case _ => Realis.ACTUAL
        }
    }
}
