package realis.rule

import java.io.File

import config.EventNuggetConfig
import doc.Doc
import event._
import org.apache.commons.cli.{CommandLine, DefaultParser, HelpFormatter, Options}
import reader.TknFilesReader
import util.{Cache, FileOps}

import scala.collection.mutable

/**
  * Created by nakayama.
  */
object RuleBasedRealisClassifier {
    def main(args: Array[String]): Unit = {
        val cl: CommandLine = parseArg(args)

        // Read configurations
        if (cl.hasOption("c")) EventNuggetConfig.set(new File(cl.getOptionValue("config")))

        // Parse args
        val dataType: Symbol =
            if (cl.hasOption("t")) {
                cl.getOptionValue("data-type") match {
                    case "train" => 'train
                    case "test" => 'predict
                    case _ => throw new IllegalArgumentException(
                        "Unknown data type (select \"train\" or \"test\"): " + cl.getOptionValue("data-type"))
                }
            } else throw new IllegalArgumentException("data type is required (select \"train\" or \"test\")")

        val tknFiles: Array[File] = //Array(new File("data/predict/data/tkn/NYT_ENG_20130523.0177.tab"))
            FileOps.getFilesFromDir(
                (if (dataType == 'train) {
                    EventNuggetConfig.tknDirForDetectionTraining
                } else {
                    EventNuggetConfig.tknDirForDetectionPrediction
                }) match {
                    case Some(d) => d
                    case _ => throw new RuntimeException("tkn directory setting was not found.")
                })
        val eventNuggetFiles: Array[File] =
            FileOps.getFilesFromDir(
                (if (dataType == 'train) {
                    EventNuggetConfig.eventNuggetDirForDetectionTraining
                } else {
                    EventNuggetConfig.eventNuggetDirForDetectionPrediction
                }) match {
                    case Some(d) => d
                    case _ => throw new RuntimeException("Event Nugget directory setting was not found.")
                })
        val outputDir: File = FileOps.withMakeParentDirs(
            if (cl.hasOption("o")) cl.getOptionValue("output-dir")
            else EventNuggetConfig.outputDir match {
                case Some(d) if d != "" => d
                case _ => throw new RuntimeException("Output directory setting was not found.")
            })

        // Parse files
        val allDoc: Array[Doc] =
            new TknFilesReader(tknFiles).parseWithEventNuggetFiles(eventNuggetFiles)
        if (EventNuggetConfig.cacheSave)
            Cache.saveDocsCache(allDoc)

        // Predict by genres
        val docsByDomain: Map[Symbol, Array[Doc]] = allDoc.groupBy(_.sourceType.getOrElse('unknown))
        val predictedDocsByDomain: Map[Symbol, Seq[PredictedEventDoc]] =
            docsByDomain.map { case (domain, docs) =>
                domain -> predict(docs)
            }

        // Calculate accuracy
        val result = new StringBuilder
        var tpSum = 0
        var fnSum = 0
        val tpSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        val fnSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        val fpSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        val tnSumByRealis: mutable.Map[Realis.Value, Int] = mutable.Map.empty[Realis.Value, Int]
        predictedDocsByDomain.foreach { case (domain, predictedDocs) =>
            result.append(s"---- ${domain.name} ----\n")

            // True Positive
            val tp: Int = predictedDocs.foldLeft(0) { (z, pd) =>
                z + pd.predictedEventNuggets.count(pen => pen.predictedRealis.orNull == pen.realis)
            }
            tpSum += tp
            // False Negative
            val fn: Int = predictedDocs.flatMap(_.predictedEventNuggets).length - tp
            fnSum += fn

            // Precision ond Recall for each realis
            Realis.values.foreach { r =>
                // Output instances
                // doc-id event-id start-offset predicted-realis event-surface url
                val outputInstances = new StringBuilder
                def eventOutputTemplate(predictedEventNugget: PredictedEventNugget) = {
                    val docId = predictedEventNugget.eventDoc.id
                    val eventId = predictedEventNugget.id.replace("em-", "E")
                    val url = s"""http://cairo.lti.cs.cmu.edu:10080/index.xhtml#/ann/${docId}?focus=${eventId}"""
                    s"${docId}\t${eventId}\t${predictedEventNugget.startOffset}\t${predictedEventNugget.predictedRealis.get.toString}\t${predictedEventNugget.text}\t${url}"
                }

                result.append(s"[REALIS]: ${r.toString}").append("\n")
                outputInstances.append(s"[REALIS]: ${r.toString}").append("\n")
                outputInstances.append(s"doc-id\tevent-id\tstart-offset\tpredicted-realis\tevent-surface\turl").append("\n")

                outputInstances.append("[true positive]\n")
                val tpByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count { pen =>
                        val isTp = pen.predictedRealis.orNull == r && pen.realis == r
                        if (isTp) {
                            outputInstances.append(eventOutputTemplate(pen)).append("\n")
                        }
                        isTp
                    }
                }
                tpSumByRealis(r) = tpSumByRealis.getOrElse(r, 0) + tpByRealis
                outputInstances.append("\n")

                outputInstances.append("[false negative]\n")
                val fnByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count { pen =>
                        val isFn = pen.realis == r && pen.predictedRealis.orNull != r
                        if (isFn) {
                            outputInstances.append(eventOutputTemplate(pen)).append("\n")
                        }
                        isFn
                    }
                }
                fnSumByRealis(r) = fnSumByRealis.getOrElse(r, 0) + fnByRealis
                outputInstances.append("\n")

                outputInstances.append("[true negative]\n")
                val tnByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count { pen =>
                        val isTn = pen.realis != r && pen.predictedRealis.orNull != r
                        if (isTn) {
                            outputInstances.append(eventOutputTemplate(pen)).append("\n")
                        }
                        isTn
                    }
                }
                tnSumByRealis(r) = tnSumByRealis.getOrElse(r, 0) + tnByRealis
                outputInstances.append("\n")

                outputInstances.append("[false positive]\n")
                val fpByRealis = predictedDocs.foldLeft(0) { (z, pd) =>
                    z + pd.predictedEventNuggets.count { pen =>
                        val isFp = pen.predictedRealis.orNull == r && pen.realis != r
                        if (isFp) {
                            outputInstances.append(eventOutputTemplate(pen)).append("\n")
                        }
                        isFp
                    }
                }
                fpSumByRealis(r) = fpSumByRealis.getOrElse(r, 0) + fpByRealis
                outputInstances.append("\n")

                // Output instances
                val instancesOutputFile = FileOps.withMakeParentDirs(
                    s"${outputDir.getAbsolutePath}/${domain.name}/${r.toString}_instances.tsv")
                FileOps.write(instancesOutputFile) {
                    _.println(outputInstances.result())
                }

                result.append(s"true  positive: ${tpByRealis}").append("\n")
                result.append(s"false negative: ${fnByRealis}").append("\n")
                result.append(s"false positive: ${fpByRealis}").append("\n")
                result.append(s"true  negative: ${tnByRealis}").append("\n")

                val precisionByRealis = BigDecimal(tpByRealis) / (BigDecimal(tpByRealis + fpByRealis) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"Precision: ").append {
                    precisionByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
                val recallByRealis = BigDecimal(tpByRealis) / (BigDecimal(tpByRealis + fnByRealis) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"Recall   : ").append {
                    recallByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
                val fByRealis = 2 * recallByRealis * precisionByRealis / ((recallByRealis + precisionByRealis) match {
                    case n if n * 2 == 0 => 1
                    case n => n
                })
                result.append(s"F-measure: ").append {
                    fByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
                }.append("\n")
            }
            result.append("\n")

            result.append("[All REALIS]").append("\n")
            result.append(s"true  positive: ${tp}").append("\n")
            result.append(s"false negative: ${fn}").append("\n")

            result.append("Accuracy: ").append {
                (BigDecimal(tp) / BigDecimal(tp + fn)) * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n\n")
        }
        result.append("---- all domain ----\n")

        // Precision ond Recall for each realis
        Realis.values.foreach { r =>
            result.append(s"[REALIS]: ${r.toString}").append("\n")

            result.append(s"true  positive: ${tpSumByRealis(r)}").append("\n")
            result.append(s"false negative: ${fnSumByRealis(r)}").append("\n")
            result.append(s"false positive: ${fpSumByRealis(r)}").append("\n")
            result.append(s"true  negative: ${tnSumByRealis(r)}").append("\n")

            val precisionByRealis = BigDecimal(tpSumByRealis(r)) / (BigDecimal(tpSumByRealis(r) + fpSumByRealis(r)) match {
                case n if n * 2 == 0 => 1
                case n => n
            })
            result.append(s"Precision: ").append {
                precisionByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n")
            val recallByRealis = BigDecimal(tpSumByRealis(r)) / (BigDecimal(tpSumByRealis(r) + fnSumByRealis(r)) match {
                case n if n * 2 == 0 => 1
                case n => n
            })
            result.append(s"Recall   : ").append {
                recallByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n")
            val fByRealis = 2 * recallByRealis * precisionByRealis / ((recallByRealis + precisionByRealis) match {
                case n if n * 2 == 0 => 1
                case n => n
            })
            result.append(s"F-measure: ").append {
                fByRealis * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
            }.append("\n")
        }
        result.append("\n")

        result.append(s"true  positive: ${tpSum}").append("\n")
        result.append(s"false negative: ${fnSum}").append("\n")
        result.append("Accuracy: ").append {
            BigDecimal(tpSum) / (BigDecimal(tpSum + fnSum) match {
                case n if n * 2 == 0 => 1
                case n => n
            }) * 100 setScale(2, BigDecimal.RoundingMode.HALF_UP)
        }

        // Output
        println(result)
        val resultFile: File = new File(s"${outputDir.getAbsolutePath}/result")
        FileOps.writeWithMakeParentDirs(resultFile)(_.println(result))
    }

    def predict(docs: Array[Doc]): Seq[PredictedEventDoc] = {
        docs.map { d =>
            // Load model and features
            val predictor = new RuleBasedPredictor
            new PredictedEventDoc(d, predictedEventNuggets = d.eventNuggets.map { en =>
                new PredictedEventNugget(en, predictedRealis = Some(predictor.predict(en)))
            })
        }
    }

    final private[this] def parseArg(args: Array[String]): CommandLine = {
        val parser = new DefaultParser()
        val options = new Options()
        options.addOption("c", "config", true, "Path to the config file.")
        options.addOption("t", "data-type", true, "Select a type of data. (train of test)")
        options.addOption("o", "output-dir", true, "Path to the output dir (not specific file).")
        options.addOption("cache", false, "Use parsing cache or not. (default false)")

        val cl = parser.parse(options, args)
        if (cl.hasOption("h")) {
            new HelpFormatter().printHelp("SentenceReconstructor", options, true)
            System.exit(0)
        }

        cl
    }
}