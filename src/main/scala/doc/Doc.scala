package doc

import edu.stanford.nlp.trees.Tree
import event.{EventHopper, EventHopperDoc, EventNugget, EventNuggetDoc}
import util.nlp.CoreNLP

/**
  * Created by nakayama.
  */
final case class Doc(
                        name: String,
                        tkns: Seq[Tkn],
                        preparedText: Option[String] = None
                    ) extends Serializable {
    def this() = this(null, null, null)

    val text: String = preparedText match {
        case Some(s) => s
        case _ => Doc.getTextByTkns(tkns)
    }

    val parseTrees: Seq[Tree] = CoreNLP(text).parseTrees

    val allTokenInParseTree: Seq[Tree] = CoreNLP(text).allTokenInParseTree

    private[this] val textFromTkn = new StringBuilder
    private[this] val textFromParseTree = new StringBuilder
    private[this] var currentTokenInParseTree: Tree = _
    private[this] val parseTreeTokenIterator: Iterator[Tree] = allTokenInParseTree.iterator
    tkns.foreach { tkn =>
        tkn.doc = this

        CoreNLP(text).tknsWithCoreNLP.get(tkn.startOffset) match {
            case Some(label) => {
                tkn.lemma = label.lemma
                tkn.pos = label.pos
                tkn.pos2 = label.pos.take(2)
                tkn.ne = label.ne match {
                    case "O" => None
                    case x => Some(x)
                }
            }
            case l =>
                new RuntimeException(s"Unexpected Stanford CoreNLP parse label: ${l.getClass.getName} (${tkn.text})")
        }

        textFromTkn.append(Range(0, tkn.startOffset - textFromTkn.length).map(_ => ' ').mkString + tkn.text)

        var counter: Iterator[Int] = Stream.from(0).iterator
        val maxCount: Int = text.length - textFromTkn.length + tkn.text.length
        def spaceComplementedTokenString(criteria: String, nodeString: String): Option[String] = {
            if (counter.next == maxCount) return None

            // Some transform rules by Stanford Core NLP Parser
            var transformedNodeString: String = nodeString

            def countContainWord(original: String, target: String): Int =
                original.sliding(target.length).toIndexedSeq.count(_.mkString == target)

            Range(0, countContainWord(criteria, "&lt;")).foreach { _ =>
                transformedNodeString = transformedNodeString.replaceFirst("<", "&lt;")
            }
            Range(0, countContainWord(criteria, "&gt;")).foreach { _ =>
                transformedNodeString = transformedNodeString.replaceFirst(">", "&gt;")
            }

            // Forward match
            if (criteria zip transformedNodeString forall {
                case (c0, c1) => c0 == c1
            }) Some(transformedNodeString)
            else spaceComplementedTokenString(criteria, " " + transformedNodeString)
        }

        def getCorrespondentTokenInParseTree: Tree = {
            if (textFromParseTree.length > tkn.startOffset) {
                currentTokenInParseTree
            } else {
                val criteria = textFromTkn.substring(textFromParseTree.length)
                var validComplementedToken: Option[String] = None
                // To avoid needless tokens like an abbreviation period which are added by Stanford Core NLP Parser
                while (validComplementedToken.isEmpty) {
                    counter = Stream.from(0).iterator
                    currentTokenInParseTree = parseTreeTokenIterator.next
                    validComplementedToken = spaceComplementedTokenString(criteria, currentTokenInParseTree.nodeString())
                }
                textFromParseTree.append(validComplementedToken.get)
                getCorrespondentTokenInParseTree
            }
        }

        tkn.nodeInParseTree = Some(getCorrespondentTokenInParseTree)
    }

    lazy val sourceType: Option[Symbol] = eventNuggetDoc orElse eventHopperDoc match {
        case Some(x) => Some(x.sourceType)
        case _ => None
    }

    lazy val eventNuggets: Seq[EventNugget] = eventNuggetDoc orElse eventHopperDoc match {
        case Some(x) => x.eventNuggets
        case _ => Seq()
    }

    lazy val eventHoppers: Option[Seq[EventHopper]] = eventHopperDoc match {
        case Some(x) => Some(x.eventHoppers)
        case _ => None
    }

    // setter and getter
    private var _eventNuggetDoc: Option[EventNuggetDoc] = _

    def eventNuggetDoc: Option[EventNuggetDoc] = _eventNuggetDoc

    def eventNuggetDoc_=(eventNuggetDoc: Option[EventNuggetDoc]) = _eventNuggetDoc = eventNuggetDoc

    private var _eventHopperDoc: Option[EventHopperDoc] = _

    def eventHopperDoc: Option[EventHopperDoc] = _eventHopperDoc

    def eventHopperDoc_=(eventHopperDoc: Option[EventHopperDoc]) = _eventHopperDoc = eventHopperDoc
}

object Doc {
    def getTextByTkns(tkns: Seq[Tkn]): String = {
        val text = new StringBuilder
        for (tkn <- tkns) {
            // fill spaces
            text.append(Range(0, tkn.startOffset - text.length).map(_ => ' ').mkString + tkn.text)
        }
        text.result
    }
}