package doc

import edu.stanford.nlp.trees.Tree

import scala.collection.JavaConversions._
import scala.collection.mutable

/**
  * Created by nakayama.
  */
case class Tkn(tknIndex: Int,
               text: String,
               startOffset: Int,
               endOffset: Int
              ) {
    private var _doc: Doc = _

    def doc: Doc = _doc

    def doc_=(doc: Doc) = _doc = doc

    lazy val nextTkn: Option[Tkn] = {
        // tknIndex starts with 1, not 0
        val nextTknIndex = tknIndex
        if (nextTknIndex < doc.tkns.length)
            Some(doc.tkns(nextTknIndex))
        else None
    }

    lazy val prevTkn: Option[Tkn] = {
        // tknIndex starts with 1, not 0
        val prevTknIndex = tknIndex - 2
        if (0 <= prevTknIndex)
            Some(doc.tkns(prevTknIndex))
        else None
    }

    private var _nodeInParseTree: Option[Tree] = _

    def nodeInParseTree: Option[Tree] = _nodeInParseTree

    def nodeInParseTree_=(nodeInParseTree: Option[Tree]) = _nodeInParseTree = nodeInParseTree

    private[this] def getSentenceRoot(node: Tree, roots: Seq[Tree]): Tree =
        roots.find(node.parent(_) != null) match {
            case Some(t) => t
            case _ => throw new RuntimeException("Root node in parse tree was not found: " + node)
        }

    lazy val sentenceNodesInParseTree: Seq[Tree] = nodeInParseTree match {
        case Some(n) => {
            val roots: Seq[Tree] = doc.parseTrees.map(_.preOrderNodeList().head)
            getSentenceRoot(n, roots).preOrderNodeList().filter(_.isLeaf).toSeq
        }
        case _ => Seq()
    }

    lazy val mainVerbInSentence: Option[Tree] = {
        val root: Option[Tree] = nodeInParseTree match {
            case Some(n) => {
                val roots: Seq[Tree] = doc.parseTrees.map(_.preOrderNodeList().head)
                Some(getSentenceRoot(n, roots))
            }
            case _ => None
        }

        def bfs(root: Tree): Option[Tree] = {
            val vQueue: mutable.Queue[Tree] = mutable.Queue[Tree](root)
            while (vQueue.nonEmpty) {
                val v = vQueue.dequeue
                if (v.nodeString.startsWith("VP [")) return Some(v)
                vQueue ++= v.children()
            }
            None
        }

        for {
            r <- root
            vp <- bfs(r)
            pos <- vp.children().headOption
        } yield pos
    }

    var lemma: String = null
    var pos: String = null
    var pos2: String = null
    var ne: Option[String] = null
}