package reader

import java.io.File

import doc.{Doc, Tkn}
import util.Cache

import scala.collection.immutable.IndexedSeq
import scala.io.Source

/**
  * Created by nakayama.
  */
object TknFileReader {
    def parse(file: File): Doc = {
        val tkns: IndexedSeq[Tkn] = parseFromText(Source.fromFile(file).getLines())
        val text: String = Doc.getTextByTkns(tkns)
        val cachedDoc: Option[Doc] = if (Cache.cacheEnable) Cache.getDocCache(text) else None
        cachedDoc getOrElse new Doc(file.getName, tkns, Some(text))
    }

    final private[this] def parseFromText(tknText: Iterator[String]): IndexedSeq[Tkn] =
        tknText.collect { case l if l != "" =>
            l.split("\t") match {
                case Array(i, t, startOffset, endOffset) => new Tkn(i.toInt, t, startOffset.toInt, endOffset.toInt)
                case s => throw new RuntimeException(s"""unexpected format (expected 4 fields tab-separated column: "${s.mkString("\t")}"""")
            }
        }.toIndexedSeq
}