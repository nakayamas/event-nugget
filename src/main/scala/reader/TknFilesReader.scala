package reader

import java.io.{File, FileNotFoundException}

import doc.Doc

/**
  * Created by nakayama.
  */
final class TknFilesReader(tknFiles: Array[File]) {
    def parseWithEventNuggetFiles(eventNuggetFiles: Array[File]): Array[Doc] =
        tknFiles.collect { case tf if tf.getName != ".tab" && tf.getName.endsWith(".tab") =>
            val tknFileName: String = tf.getName
            val docId: String = tknFileName.substring(0, tknFileName.lastIndexOf(".tab"))

            // Parse tkn file
            val doc: Doc = TknFileReader.parse(tf)

            // Parse event nugget file
            val expectedEventNuggetFileName = docId + ".event_nuggets.xml"
            val eventNuggetFile = eventNuggetFiles.find(f => f.getName == expectedEventNuggetFileName) match {
                case Some(x) => x
                case _ => throw new FileNotFoundException(expectedEventNuggetFileName + " is not found.")
            }
            // Add event nugget data to tkn data
            doc.eventNuggetDoc = Some(EventNuggetFileReader(doc).parse(eventNuggetFile))

            doc
        }

    def parseWithEventHopperFiles(eventHopperFiles: Array[File]): Array[Doc] =
        tknFiles.collect { case tf if tf.getName != ".tab" && tf.getName.endsWith(".tab") =>
            val tknFileName: String = tf.getName
            val docId: String = tknFileName.substring(0, tknFileName.lastIndexOf(".tab"))

            // Parse tkn file
            val doc: Doc = TknFileReader.parse(tf)

            // Parse event nugget file
            val expectedEventHopperFileName = docId + ".event_hoppers.xml"
            val eventHopperFile = eventHopperFiles.find(f => f.getName == expectedEventHopperFileName) match {
                case Some(x) => x
                case _ => throw new FileNotFoundException(expectedEventHopperFileName + " is not found.")
            }
            // Add event nugget data to tkn data
            doc.eventHopperDoc = Some(EventHopperFileReader(doc).parse(eventHopperFile))

            doc
        }
}
