package reader

import java.io.File

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

/**
  * Created by nakayama.
  */
object EventNuggetStatsReader {

    def eventNuggetStatsFileParser(genreFile: File): Map[String, String] = {
        var genres: ArrayBuffer[String] = ArrayBuffer()
        val fileName2Genre: mutable.Map[String, String] = mutable.Map.empty[String, String]
        val s = Source.fromFile(genreFile)
        try {
            for {
                line <- s.getLines if line != "file	genre	word_count	hoppers	event_mentions"
            } {
                line.split("\t") match {
                    case Array(fileName, genre, _, _, _) =>
                        genres += genre
                        fileName2Genre(fileName) = genre
                }
            }
        } finally {
            s.close
        }

        fileName2Genre.toMap
    }

}
