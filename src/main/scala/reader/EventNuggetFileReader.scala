package reader

import java.io.File

import doc.Doc
import event.{EventNugget, EventNuggetDoc, Realis}

import scala.xml.XML

/**
  * Created by nakayama.
  */
final class EventNuggetFileReader(doc: Doc) {
    def parse(enFile: File): EventNuggetDoc = parse(XML.loadFile(enFile))

    def parse[EventType, EventSubtype, Realis](enx: xml.Elem): EventNuggetDoc = {
        // <root>
        val infoNode = enx \\ "deft_ere_event_nuggets_only"
        val docId = (enx \\ "deft_ere_event_nuggets_only" \ "@doc_id").text
        val sourceType = Symbol((enx \\ "deft_ere_event_nuggets_only" \ "@source_type").text)
        // <event_mention>
        val eventNuggets: Seq[EventNugget] = (enx \\ "event_mention").map { em =>
            val eventNuggetId = (em \ "@id").text
            val eventType = Symbol((em \ "@type").text)
            val eventSubtype = Symbol((em \ "@subtype").text)
            val realis = Realis.withName((em \ "@realis").text.toUpperCase)
            val formality = Symbol((em \ "@formality").text)
            val schedule = Symbol((em \ "@schedule").text)
            val medium = Symbol((em \ "@medium").text)
            val audience = Symbol((em \ "@audience").text)
            val trigger = em \ "trigger"
            val offset = (trigger \ "@offset").text.toInt
            val length = (trigger \ "@length").text.toInt
            val text = trigger.text
            new EventNugget(eventNuggetId, eventType, eventSubtype, realis, formality, schedule, medium, audience, offset, length, text)
        }
        new EventNuggetDoc(doc, sourceType, eventNuggets.sortBy(_.startOffset))
    }
}

object EventNuggetFileReader {
    def apply(doc: Doc): EventNuggetFileReader = new EventNuggetFileReader(doc)
}